import os
import sys
import h5py
import numpy as np
from datetime import datetime
from scipy.io import readsav
from scipy import interpolate

class READ_HSK:
    def __init__(self, fname, skip_header=66):
        # read in aircraft house-keeping file
        hsk_data = np.genfromtxt(fname, skip_header=66, delimiter=',')
        self.tmhr    = hsk_data[:, 0]/3600.0
        self.lon     = hsk_data[:, 3]
        self.lat     = hsk_data[:, 2]
        self.alt     = hsk_data[:, 4]
        self.ang_pit = hsk_data[:, 14]
        self.ang_rol = hsk_data[:, 15]
        self.ang_head= hsk_data[:, 12]

if __name__ == '__main__':
    import matplotlib.pyplot as plt
    from matplotlib.ticker import FixedLocator
    fname = '/data/hong/data/arise/hsk/ARISE-C130-Hskping_c130_20141002_R1.ict'
    data_hsk = READ_HSK(fname)
    print(data_hsk.tmhr)

