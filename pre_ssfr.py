import os
import glob
import h5py
import struct
import numpy as np
import datetime
from scipy.io import readsav
from scipy import stats
import pysolar

# +++++++++++++++++++++++++    for .SKS files   ++++++++++++++++++++++++++++++
def READ_SKS_ONE_V1(fname, headLen=148, dataLen=2260, verbose=False):

    fileSize = os.path.getsize(fname)
    if fileSize > headLen:
        iterN   = (fileSize-headLen) // dataLen
        residual = (fileSize-headLen) %  dataLen
        if residual != 0:
            print('Warning [READ_SKS_ONE_V1]: %s has invalid data size, return empty arrays.' % fname)
            iterN = 0
            comment    = []
            spectra    = np.zeros((iterN, 256, 4), dtype=np.float64) # spectra
            shutter    = np.zeros(iterN          , dtype=np.int32  ) # shutter status (1:closed, 0:open)
            int_time   = np.zeros((iterN, 4)     , dtype=np.float64) # integration time [ms]
            temp       = np.zeros((iterN, 9)     , dtype=np.float64) # temperature
            qual_flag  = np.ones(iterN           , dtype=np.int32)   # quality flag (1:good, 0:bad)
            jday_NSF   = np.zeros(iterN          , dtype=np.float64)
            jday_cRIO  = np.zeros(iterN          , dtype=np.float64)
            return comment, spectra, shutter, int_time, temp, jday_NSF, jday_cRIO, qual_flag, iterN
    else:
        print('Warning [READ_SKS_ONE_V1]: %s has file size smaller than header, return empty arrays.' % fname)
        iterN = 0
        comment    = []
        spectra    = np.zeros((iterN, 256, 4), dtype=np.float64) # spectra
        shutter    = np.zeros(iterN          , dtype=np.int32  ) # shutter status (1:closed, 0:open)
        int_time   = np.zeros((iterN, 4)     , dtype=np.float64) # integration time [ms]
        temp       = np.zeros((iterN, 9)     , dtype=np.float64) # temperature
        qual_flag  = np.ones(iterN           , dtype=np.int32)   # quality flag (1:good, 0:bad)
        jday_NSF   = np.zeros(iterN          , dtype=np.float64)
        jday_cRIO  = np.zeros(iterN          , dtype=np.float64)
        return comment, spectra, shutter, int_time, temp, jday_NSF, jday_cRIO, qual_flag, iterN

    spectra    = np.zeros((iterN, 256, 4), dtype=np.float64) # spectra
    shutter    = np.zeros(iterN          , dtype=np.int32  ) # shutter status (1:closed, 0:open)
    int_time   = np.zeros((iterN, 4)     , dtype=np.float64) # integration time [ms]
    temp       = np.zeros((iterN, 9)     , dtype=np.float64) # temperature
    qual_flag  = np.ones(iterN           , dtype=np.int32)   # quality flag (1:good, 0:bad)
    jday_NSF   = np.zeros(iterN          , dtype=np.float64)
    jday_cRIO  = np.zeros(iterN          , dtype=np.float64)

    f           = open(fname, 'rb')
    # read head
    headRec   = f.read(headLen)
    head      = struct.unpack('<B144s3B', headRec)
    if head[0] != 144:
        f.seek(0)
    else:
        comment = head[1]

    if verbose:
        print('++++++++++++++++++++++++++++++++++++++++++++++++++')
        print('Comments in %s...' % fname.split('/')[-1])
        print(comment)
        print('--------------------------------------------------')

    # read data record
    for i in range(iterN):
        dataRec = f.read(dataLen)
        # ---------------------------------------------------------------------------------------------------------------
        # d9l: frac_second[d] , second[l] , minute[l] , hour[l] , day[l] , month[l] , year[l] , dow[l] , doy[l] , DST[l]
        # d9l: frac_second0[d], second0[l], minute0[l], hour0[l], day0[l], month0[l], year0[l], dow0[l], doy0[l], DST0[l]
        # l9d: null[l], temp(9)[9d]
        # --------------------------          below repeat for sz, sn, iz, in          ----------------------------------
        # l2Bl: int_time[l], shutter[B], EOS[B], null[l]
        # 257h: spectra(257)
        # ---------------------------------------------------------------------------------------------------------------
        data     = struct.unpack('<d9ld9ll9dl2Bl257hl2Bl257hl2Bl257hlBBl257h', dataRec)

        dataHead = data[:30]
        dataSpec = np.transpose(np.array(data[30:]).reshape((4, 261)))[:, [0, 2, 1, 3]]
        # [0, 2, 1, 3]: change order from 'sz, sn, iz, in' to 'sz, iz, sn, in'
        # transpose: change shape from (4, 261) to (261, 4)

        shutter_logic = (np.unique(dataSpec[1, :]).size != 1)
        eos_logic     = any(dataSpec[2, :] != 1)
        null_logic    = any(dataSpec[3, :] != 257)
        order_logic   = not np.array_equal(dataSpec[4, :], np.array([0, 2, 1, 3]))
        if any([shutter_logic, eos_logic, null_logic, order_logic]):
            qual_flag[i] = 0

        if True:
            spectra[i, :, :]  = dataSpec[5:, :]
            shutter[i]        = dataSpec[1, 0]
            int_time[i, :]    = dataSpec[0, :]
            temp[i, :]        = dataHead[21:]

            dtime          = datetime.datetime(dataHead[6] , dataHead[5] , dataHead[4] , dataHead[3] , dataHead[2] , dataHead[1] , int(round(dataHead[0]*1000000.0)))
            dtime0         = datetime.datetime(dataHead[16], dataHead[15], dataHead[14], dataHead[13], dataHead[12], dataHead[11], int(round(dataHead[10]*1000000.0)))

            # calculate the proleptic Gregorian ordinal of the date
            jday_NSF[i]    = (dtime  - datetime.datetime(1, 1, 1)).total_seconds() / 86400.0 + 1.0
            jday_cRIO[i]   = (dtime0 - datetime.datetime(1, 1, 1)).total_seconds() / 86400.0 + 1.0

    return comment, spectra, shutter, int_time, temp, jday_NSF, jday_cRIO, qual_flag, iterN

class READ_SKS:

    def __init__(self, fnames, dateRef=None, Ndata=600, secOffset=0.0, verbose=False):

        if type(fnames) is not list:
            exit('Error [READ_SKS.__init__]: input variable "fnames" should be in list type.')

        Nx         = Ndata * len(fnames)
        comment    = []
        spectra    = np.zeros((Nx, 256, 4), dtype=np.float64) # spectra
        shutter    = np.zeros(Nx          , dtype=np.int32  ) # shutter status (1:closed, 0:open)
        int_time   = np.zeros((Nx, 4)     , dtype=np.float64) # integration time [ms]
        temp       = np.zeros((Nx, 9)     , dtype=np.float64) # temperature
        qual_flag  = np.zeros(Nx          , dtype=np.int32)
        jday_NSF   = np.zeros(Nx          , dtype=np.float64)
        jday_cRIO  = np.zeros(Nx          , dtype=np.float64)

        Nstart = 0
        if verbose:
            print('+++++++++++++++++++ Reading SKS files ++++++++++++++++++++')
        for fname in fnames:
            if verbose:
                print('Reading %s...' % fname)

            comment0, spectra0, shutter0, int_time0, temp0, jday_NSF0, jday_cRIO0, qual_flag0, iterN0 = READ_SKS_ONE_V1(fname)
            comment.append(comment0)

            Nend = iterN0 + Nstart

            spectra[Nstart:Nend, ...]    = spectra0
            shutter[Nstart:Nend, ...]    = shutter0
            int_time[Nstart:Nend, ...]   = int_time0
            temp[Nstart:Nend, ...]       = temp0
            jday_NSF[Nstart:Nend, ...]   = jday_NSF0
            jday_cRIO[Nstart:Nend, ...]  = jday_cRIO0
            qual_flag[Nstart:Nend, ...]  = qual_flag0

            Nstart = Nend

        comment    = comment
        spectra    = spectra[:Nend, ...]
        shutter    = shutter[:Nend, ...]
        int_time   = int_time[:Nend, ...]
        temp       = temp[:Nend, ...]
        jday_NSF   = jday_NSF[:Nend, ...]
        jday_cRIO  = jday_cRIO[:Nend, ...]
        qual_flag  = qual_flag[:Nend, ...]

        if dateRef == None:
            jdayRef = int(jday_NSF[30])
            self.dateRef = datetime.datetime(1, 1, 1)+datetime.timedelta(seconds=(jdayRef-1.0)*86400.0)
        else:
            jdayRef = (dateRef -(datetime.datetime(1, 1, 1))).days + 1.0
            self.dateRef = dateRef

        logic = (jday_NSF-jdayRef>=0.0)&(jday_NSF-jdayRef<=2.0)
        self.spectra    = spectra[logic, ...]
        self.shutter    = shutter[logic, ...]
        self.int_time   = int_time[logic, ...]
        self.temp       = temp[logic, ...]
        self.jday_NSF   = jday_NSF[logic, ...]
        self.jday_cRIO  = jday_cRIO[logic, ...]
        self.qual_flag  = qual_flag[logic, ...]

        self.jday = self.jday_NSF.copy()
        #  self.jday = self.jday_cRIO[0] + 0.5/86400.0 * np.arange(self.jday_cRIO.size)
        self.tmhr = (self.jday - jdayRef) * 24.0
        self.shutter_ori= self.shutter.copy()

        self.secOffset = secOffset
        self.jday_corr = self.jday - secOffset/86400.0
        self.tmhr_corr = self.tmhr - secOffset/3600.0

        if verbose:
            print('----------------------------------------------------------')

        if verbose:
            print('+++++++++++++++++++Starting correction++++++++++++++++++++')
            print('Dark correction...')
        self.DARK_CORR()

        #if verbose:
            #print('Non-linearity correction...')
        #self.spectra_nlin_corr = self.spectra_dark_corr.copy()
        #fname_nlin ='/argus/home/chen/work/02_arise/06_cal/aux/20141121.sav'
        #Nsen       = 1
        #self.NLIN_CORR(fname_nlin, Nsen)
        #fname_nlin ='/argus/home/chen/work/02_arise/06_cal/aux/20141119.sav'
        #Nsen       = 3
        #self.NLIN_CORR(fname_nlin, Nsen)

        #if verbose:
            #print('Cosine correction...')
        #fnames    = sorted(glob.glob('/argus/field/arise/C130/%s/*.plt2' % self.dateRef.strftime('%Y%m%d')))
        #data_plt  = READ_PLT2(fnames, dateRef=self.dateRef, secOffset=17.1)
        #self.COS_CORR(data_plt)
        #self.COUNT2FLUX(self.spectra_nlin_corr)

        if verbose:
            print('----------------------------------------------------------')

    def DARK_CORR(self, mode=-1, darkExtend=2, lightExtend=2, countOffset=0, lightThr=10, darkThr=5):

        if self.shutter[0] == 0:
            darkL = np.array([], dtype=np.int32)
            darkR = np.array([0], dtype=np.int32)
        else:
            darkR = np.array([], dtype=np.int32)
            darkL = np.array([0], dtype=np.int32)

        darkL0 = np.squeeze(np.argwhere((self.shutter[1:]-self.shutter[:-1]) ==  1)) + 1
        darkL  = np.hstack((darkL, darkL0))

        darkR0 = np.squeeze(np.argwhere((self.shutter[1:]-self.shutter[:-1]) == -1)) + 1
        darkR  = np.hstack((darkR, darkR0))

        if self.shutter[-1] == 0:
            darkL = np.hstack((darkL, self.shutter.size))
        else:
            darkR = np.hstack((darkR, self.shutter.size))

        self.spectra_dark_corr = self.spectra.copy() + countOffset
        self.dark_offset       = np.zeros(self.spectra.shape, dtype=np.float64)
        self.dark_std          = np.zeros(self.spectra.shape, dtype=np.float64)

        Nrecord, Nchannel, Nsensor = self.spectra.shape
        if mode == -1:
            if darkL.size-darkR.size==0:
                if darkL[0]>darkR[0] and darkL[-1]>darkR[-1]:
                    darkL = darkL[:-1]
                    darkR = darkR[1:]
            elif darkL.size-darkR.size==1:
                if darkL[0]>darkR[0] and darkL[-1]<darkR[-1]:
                    darkL = darkL[1:]
                elif darkL[0]<darkR[0] and darkL[-1]>darkR[-1]:
                    darkL = darkL[:-1]
            elif darkR.size-darkL.size==1:
                if darkL[0]>darkR[0] and darkL[-1]<darkR[-1]:
                    darkR = darkR[1:]
                elif darkL[0]<darkR[0] and darkL[-1]>darkR[-1]:
                    darkR = darkR[:-1]
            else:
                exit('Error [READ_SKS.DARK_CORR]: darkL and darkR are wrong.')

            for i in range(darkL.size-1):
                if darkR[i] < darkL[i]:
                    exit('Error [READ_SKS.DARK_CORR]: darkL > darkR.')

                darkLL = darkL[i] + darkExtend
                darkLR = darkR[i] - darkExtend
                darkRL = darkL[i+1] + darkExtend
                darkRR = darkR[i+1] - darkExtend

                if i == 0:
                    self.shutter[:darkLL] = -1  # omit the data before the first dark cycle

                lightL = darkR[i]   + lightExtend
                lightR = darkL[i+1] - lightExtend

                if lightR-lightL>lightThr and darkLR-darkLL>darkThr and darkRR-darkRL>darkThr:

                    self.shutter[darkL[i]:darkLL] = -1
                    self.shutter[darkLR:darkR[i]] = -1
                    self.shutter[darkR[i]:lightL] = -1
                    self.shutter[lightR:darkL[i+1]] = -1
                    self.shutter[darkL[i+1]:darkRL] = -1
                    self.shutter[darkRR:darkR[i+1]] = -1

                    int_dark  = np.append(self.int_time[darkLL:darkLR], self.int_time[darkRL:darkRR]).mean()
                    int_light = self.int_time[lightL:lightR].mean()

                    if np.abs(int_dark - int_light) > 0.0001:
                        self.shutter[lightL:lightR] = -1
                    else:
                        interp_x  = np.append(self.tmhr[darkLL:darkLR], self.tmhr[darkRL:darkRR])
                        if i==darkL.size-2:
                            target_x  = self.tmhr[darkLL:darkRR]
                        else:
                            target_x  = self.tmhr[darkLL:darkRL]

                        for ichan in range(Nchannel):
                            for isen in range(Nsensor):
                                interp_y = np.append(self.spectra[darkLL:darkLR,ichan,isen], self.spectra[darkRL:darkRR,ichan,isen])
                                slope, intercept, r_value, p_value, std_err  = stats.linregress(interp_x, interp_y)
                                if i==darkL.size-2:
                                    self.dark_offset[darkLL:darkRR, ichan, isen] = target_x*slope + intercept
                                    self.spectra_dark_corr[darkLL:darkRR, ichan, isen] -= self.dark_offset[darkLL:darkRR, ichan, isen]
                                    self.dark_std[darkLL:darkRR, ichan, isen] = np.std(interp_y)
                                else:
                                    self.dark_offset[darkLL:darkRL, ichan, isen] = target_x*slope + intercept
                                    self.spectra_dark_corr[darkLL:darkRL, ichan, isen] -= self.dark_offset[darkLL:darkRL, ichan, isen]
                                    self.dark_std[darkLL:darkRL, ichan, isen] = np.std(interp_y)

                else:
                    self.shutter[darkL[i]:darkR[i+1]] = -1

            self.shutter[darkRR:] = -1  # omit the data after the last dark cycle

        elif mode == -2:
            print('Message [DARK_CORR]: Not implemented...')

        elif mode == -3:

            #if darkL.size-darkR.size==0:
                #if darkL[0]>darkR[0] and darkL[-1]>darkR[-1]:
                    #darkL = darkL[:-1]
                    #darkR = darkR[1:]
            #elif darkL.size-darkR.size==1:
                #if darkL[0]>darkR[0] and darkL[-1]<darkR[-1]:
                    #darkL = darkL[1:]
                #elif darkL[0]<darkR[0] and darkL[-1]>darkR[-1]:
                    #darkL = darkL[:-1]
            #elif darkR.size-darkL.size==1:
                #if darkL[0]>darkR[0] and darkL[-1]<darkR[-1]:
                    #darkR = darkR[1:]
                #elif darkL[0]<darkR[0] and darkL[-1]>darkR[-1]:
                    #darkR = darkR[:-1]
            #else:
                #exit('Error [READ_SKS.DARK_CORR]: darkL and darkR are wrong.')

            for i in range(darkR.size):
                darkLL = darkL[i]   + darkExtend
                darkLR = darkR[i]   - darkExtend
                lightL = darkR[i]   + lightExtend
                lightR = darkL[i+1] - lightExtend

                self.shutter[darkL[i]:darkLL] = -1
                self.shutter[darkLR:darkR[i]] = -1
                self.shutter[darkR[i]:lightL] = -1
                self.shutter[lightR:darkL[i+1]] = -1

                int_dark  = self.int_time[darkLL:darkLR].mean()
                int_light = self.int_time[lightL:lightR].mean()
                if np.abs(int_dark - int_light) > 0.0001:
                    self.shutter[lightL:lightR] = -1
                    exit('Error [READ_SKS.DARK_CORR]: inconsistent integration time.')
                else:
                    for itmhr in range(darkLR, lightR):
                        for isen in range(Nsensor):
                            dark_offset0 = np.mean(self.spectra[darkLL:darkLR, :, isen], axis=0)
                            self.dark_offset[itmhr, :, isen] = dark_offset0
                    self.spectra_dark_corr[lightL:lightR,:,:] -= self.dark_offset[lightL:lightR,:,:]

        elif mode == -4:
            print('Message [DARK_CORR]: Not implemented...')

    def NLIN_CORR(self, fname_nlin, Nsen, verbose=False):

        int_time0 = np.mean(self.int_time[:, Nsen])
        f_nlin = readsav(fname_nlin)

        if abs(f_nlin.iin_-int_time0)>1.0e-5:
            exit('Error [READ_SKS]: Integration time do not match.')

        for iwvl in range(256):
            xx0   = self.spectra_nlin_corr[:,iwvl,Nsen].copy()
            xx    = np.zeros_like(xx0)
            yy    = np.zeros_like(xx0)
            #self.spectra_nlin_corr[:,iwvl,Nsen] = np.nan
            self.spectra_nlin_corr[:,iwvl,Nsen] = 0.0
            logic_xx     = (xx0>-100)
            if verbose:
                print('++++++++++++++++++++++++++++++++++++++++++++++++')
                print('range', f_nlin.mxm_[0,iwvl]*f_nlin.in_[iwvl], f_nlin.mxm_[1,iwvl]*f_nlin.in_[iwvl])
                print('good', logic_xx.sum(), xx0.size)
            xx0[logic_xx] = xx0[logic_xx]/f_nlin.in_[iwvl]

            if (f_nlin.bad_[1,iwvl]<1.0) and (f_nlin.mxm_[0,iwvl]>=1.0e-3):

                #+ data in range (0, minimum)
                yy_e = 0.0
                for ideg in range(f_nlin.gr_):
                    yy_e += f_nlin.res2_[ideg,iwvl]*f_nlin.mxm_[0,iwvl]**ideg
                slope = yy_e/f_nlin.mxm_[0,iwvl]
                logic_xx     = (xx0>-100) & (xx0<f_nlin.mxm_[0,iwvl])
                if verbose:
                    print('0-min', logic_xx.sum(), xx0.size)
                    print('data', xx0[logic_xx])
                xx[xx<0]     = 0.0
                xx[logic_xx] = xx0[logic_xx]
                yy[logic_xx] = xx[logic_xx]*slope

                self.spectra_nlin_corr[logic_xx,iwvl,Nsen] = yy[logic_xx]*f_nlin.in_[iwvl]
                #-

                #+ data in range [minimum, maximum]
                logic_xx     = (xx0>=f_nlin.mxm_[0,iwvl]) & (xx0<=f_nlin.mxm_[1,iwvl])
                xx[logic_xx] = xx0[logic_xx]
                if verbose:
                    print('min-max', logic_xx.sum(), xx0.size)
                    print('------------------------------------------------')
                for ideg in range(f_nlin.gr_):
                    yy[logic_xx] += f_nlin.res2_[ideg, iwvl]*xx[logic_xx]**ideg

                self.spectra_nlin_corr[logic_xx,iwvl,Nsen] = yy[logic_xx]*f_nlin.in_[iwvl]
                #-

    def COS_CORR(self, data_plt,
                 fname_zen = '/argus/home/chen/work/02_arise/06_cal/pro/03_cos_corr/20150221_LCx_zenith.out',
                 fname_nad = '/argus/home/chen/work/02_arise/06_cal/pro/03_cos_corr/20150221_LCx_nadir.out'):

        #iza, iaa = PRH2ZA(data_plt.ang_pit_a-data_plt.ang_pit_m, data_plt.ang_rol_a-data_plt.ang_rol_m, data_plt.ang_head)
        iza, iaa = PRH2ZA(data_plt.ang_pit-data_plt.ang_pit_m, data_plt.ang_rol-data_plt.ang_rol_m, data_plt.ang_head)

        tmhrMin = self.tmhr_corr.min()
        tmhrMax = self.tmhr_corr.max()

        if data_plt.tmhr_corr.min() > tmhrMin or data_plt.tmhr_corr.max() < tmhrMax:
            exit('Error [READ_SKS.COS_CORR]: GPS time range from INS cannot include SSFR time range to be interpolated.')

        logic = (data_plt.tmhr_corr>=tmhrMin) & (data_plt.tmhr_corr<=tmhrMax)

        iza0  = np.interp(self.tmhr_corr, data_plt.tmhr_corr[logic], iza[logic])
        iaa0  = np.interp(self.tmhr_corr, data_plt.tmhr_corr[logic], iaa[logic])

        lon0  = np.interp(self.tmhr_corr, data_plt.tmhr_corr[logic], data_plt.lon[logic])
        lat0  = np.interp(self.tmhr_corr, data_plt.tmhr_corr[logic], data_plt.lat[logic])
        alt0  = np.interp(self.tmhr_corr, data_plt.tmhr_corr[logic], data_plt.alt[logic])

        # calculate solar zenith angle (sza), solar azimuth angle (saa)
        sza0 = np.zeros_like(self.tmhr_corr)
        saa0 = np.zeros_like(self.tmhr_corr)
        for i in range(lon0.size):
            dtime_i  = self.dateRef + datetime.timedelta(hours=self.tmhr_corr[i])
            sza0[i] = 90.0 - pysolar.solar.get_altitude(lat0[i], lon0[i], dtime_i, elevation=alt0[i])
            saa_i = pysolar.get_azimuth(lat0[i], lon0[i], dtime_i, elevation=alt0[i])
            if saa_i >= 0:
                if 0.0<=saa_i<=180.0:
                    saa_i = 180.0 - saa_i
                elif 180.0<saa_i<=360.0:
                    saa_i = 540.0 - saa_i
                else:
                    print('Warning [READ_SKS.COS_CORR]: invalid solar azimuth angle.')
            elif saa_i < 0:
                if -180.0<=saa_i<0.0:
                    saa_i = -saa_i + 180.0
                elif -360.0<=saa_i<-180.0:
                    saa_i = -saa_i - 180.0
                else:
                    print('Warning [READ_SKS.COS_CORR]: invalid solar azimuth angle.')
            saa0[i] = saa_i

        dc     = MUSLOPE(sza0, saa0, iza0, iaa0)

        indice = np.int_(np.round(dc, decimals=3)*1000.0)
        indice[indice>1000]=1000
        indice[indice<0]   =0

        f_cos = readsav(fname_zen)
        wvl   = f_cos.wvlz
        resp  = f_cos.res_z_wvl
        mu_i  = f_cos.mu_interpol

        wvl0   = 666.0
        index0 = np.argmin(np.abs(wvl-wvl0))
        resp0  = resp[index0, :] - 0.008
        #resp0  = resp[index0, :]
        #resp0  = resp[index0, :] + 0.013
        resp0[resp0<0.0000001] = 0.0000001

        self.cos_corr_factor = np.cos(np.deg2rad(sza0)) / resp0[indice]

    def COUNT2FLUX(self, countIn):

        f_ns = readsav('/argus/home/chen/work/02_arise/06_cal/aux/20140924_s1n_150B_s300.sav')
        f_ni = readsav('/argus/home/chen/work/02_arise/06_cal/aux/20140924_s1n_150B_i300.sav')
        f_zs = readsav('/argus/home/chen/work/02_arise/06_cal/aux/20140921_s1z_150B_s300.sav')
        f_zi = readsav('/argus/home/chen/work/02_arise/06_cal/aux/20140921_s1z_150B_i300.sav')

        logic_nsi2 = (f_ns.wl_si2 <= f_ni.join)
        logic_nin2 = (f_ni.wl_in2 >= f_ni.join)
        nsi2 = logic_nsi2.sum()
        nin2 = logic_nin2.sum()
        n2 = nsi2 + nin2

        logic_nsi1 = (f_zs.wl_si1 <= f_ni.join)
        logic_nin1 = (f_zi.wl_in1 >= f_ni.join)
        nsi1 = logic_nsi1.sum()
        nin1 = logic_nin1.sum()
        n1 = nsi1 + nin1

        f_ns.resp2_si2[f_ns.resp2_si2<1.0e-10] = -1
        f_ni.resp2_in2[f_ni.resp2_in2<1.0e-10] = -1
        f_zs.resp2_si1[f_zs.resp2_si1<1.0e-10] = -1
        f_zi.resp2_in1[f_zi.resp2_in1<1.0e-10] = -1

        self.wvl_zen = np.append(f_zs.wl_si1[logic_nsi1], f_zi.wl_in1[logic_nin1][::-1])
        self.wvl_nad = np.append(f_ns.wl_si2[logic_nsi2], f_ni.wl_in2[logic_nin2][::-1])
        self.spectra_flux_zen = np.zeros((self.tmhr_corr.size, n1), dtype=np.float64)
        self.spectra_flux_nad = np.zeros((self.tmhr_corr.size, n2), dtype=np.float64)
        for i in range(self.tmhr_corr.size):
            if self.shutter[i] == 0:
                self.spectra_flux_zen[i, :nsi1] = countIn[i, logic_nsi1, 0]/float(self.int_time[i, 0])/f_zs.resp2_si1[logic_nsi1]
                self.spectra_flux_zen[i, nsi1:] = (countIn[i, logic_nin1, 1]/float(self.int_time[i, 1])/f_zi.resp2_in1[logic_nin1])[::-1]
                self.spectra_flux_nad[i, :nsi2] = countIn[i, logic_nsi2, 2]/float(self.int_time[i, 2])/f_ns.resp2_si2[logic_nsi2]
                self.spectra_flux_nad[i, nsi2:] = (countIn[i, logic_nin2, 3]/float(self.int_time[i, 3])/f_ni.resp2_in2[logic_nin2])[::-1]
            #else:
                #self.spectra_flux_zen[i, :] = np.nan
                #self.spectra_flux_nad[i, :] = np.nan

        self.spectra_flux_zen[self.spectra_flux_zen<0.0] = 0.0
        self.spectra_flux_nad[self.spectra_flux_nad<0.0] = 0.0

class READ_SKS_TEST:

    def __init__(self, fnames, dateRef=None, Ndata=600, secOffset=0.0, verbose=False):

        f = h5py.File('test_cir1_raw.h5', 'r')
        # f = h5py.File('test_cir2_raw.h5', 'r')
        self.spectra           = f['spectra'][...]
        self.shutter           = f['shutter'][...]
        self.int_time          = f['int_time'][...]
        self.temp              = f['temp'][...]
        self.jday_NSF          = f['jday_NSF'][...]
        self.jday_cRIO         = f['jday_cRIO'][...]
        self.qual_flag         = f['qual_flag'][...]
        self.jday_corr         = f['jday_corr'][...]
        self.tmhr_corr         = f['tmhr_corr'][...]
        self.spectra_dark_corr = f['spectra_dark_corr'][...]
        f.close()

        if dateRef == None:
            jdayRef = int(self.jday_NSF[30])
            self.dateRef = datetime.datetime(1, 1, 1)+datetime.timedelta(seconds=(jdayRef-1.0)*86400.0)
        else:
            jdayRef = (dateRef -(datetime.datetime(1, 1, 1))).days + 1.0
            self.dateRef = dateRef

        if verbose:
            print('----------------------------------------------------------')

        if verbose:
            print('+++++++++++++++++++Starting correction++++++++++++++++++++')
            print('Dark correction...')

        if verbose:
            print('Non-linearity correction...')
        self.spectra_nlin_corr = self.spectra_dark_corr.copy()
        fname_nlin ='data/ssfr/aux/20141121.sav'
        Nsen       = 1
        self.NLIN_CORR(fname_nlin, Nsen)
        fname_nlin ='data/ssfr/aux/20141119.sav'
        Nsen       = 3
        self.NLIN_CORR(fname_nlin, Nsen)

        # if verbose:
        #     print('Cosine correction...')
        fnames    = sorted(glob.glob('data/ssfr/*.plt2'))
        # data_plt  = READ_PLT2(fnames, dateRef=self.dateRef, secOffset=16.1)
        data_plt  = READ_PLT2(fnames, dateRef=self.dateRef, secOffset=20.0)
        self.COS_CORR(data_plt)
        self.COUNT2FLUX(self.spectra_nlin_corr)

        self.spectra_flux_zen[self.rol_filter, :] = -1.0
        self.spectra_flux_nad[self.rol_filter, :] = -1.0

        index = np.argmin(np.abs(self.wvl_zen-666.0))
        self.flux_ori      = self.spectra_flux_zen[:, index]
        self.flux_corr     = self.flux_ori * self.cos_corr_factor
        self.flux_corr_ori = self.flux_ori * self.cos_corr_factor_ori

        self.cos_corr_factor0 = self.cos_corr_factor[index]
        self.cos_corr_factor_ori0 = self.cos_corr_factor[index]

        # figure settings
        # fig = plt.figure(figsize=(8, 6))
        # ax1 = fig.add_subplot(111)
        # ax1.set_ylim((0.0, 0.2))

        # colors = plt.cm.jet(np.linspace(0.0, 1.0, self.tmhr_corr.size))
        # for i in range(self.tmhr_corr.size):
        #     logic_zen = (self.spectra_flux_zen[i, :] < 0.0)
        #     logic_nad = (self.spectra_flux_nad[i, :] < 0.0)
        #     if 0 < logic_zen.sum() < self.wvl_zen.size or 0 < logic_nad.sum() < self.wvl_nad.size:
        #         print('oh no')
        #     elif logic_zen.sum()==0:
        #         # ax1.scatter(self.wvl_zen, self.spectra_flux_zen[i, :], c=colors[i, :], s=0.1, alpha=0.1)
        #         ax1.scatter(self.wvl_nad, self.spectra_flux_nad[i, :], c=colors[i, :], s=0.1, alpha=0.1)
        #         # ax1.scatter(self.wvl_nad, self.spectra_flux_nad[i, :], c=colors[i], s=0.1, alpha=0.1)
        # # plt.scatter(self.tmhr_corr, self.flux_corr, s=1.0)
        # # ax1.set_ylim((0.0, 1.2))
        # # ax1.legend(loc='best', fontsize=12, framealpha=0.4)
        # plt.savefig('nad_spectra.png')
        # plt.show()
        # exit()

        if verbose:
            print('----------------------------------------------------------')

    def DARK_CORR(self, mode=-1, darkExtend=2, lightExtend=2, countOffset=0, lightThr=10, darkThr=5):

        if self.shutter[0] == 0:
            darkL = np.array([], dtype=np.int32)
            darkR = np.array([0], dtype=np.int32)
        else:
            darkR = np.array([], dtype=np.int32)
            darkL = np.array([0], dtype=np.int32)

        darkL0 = np.squeeze(np.argwhere((self.shutter[1:]-self.shutter[:-1]) ==  1)) + 1
        darkL  = np.hstack((darkL, darkL0))

        darkR0 = np.squeeze(np.argwhere((self.shutter[1:]-self.shutter[:-1]) == -1)) + 1
        darkR  = np.hstack((darkR, darkR0))

        if self.shutter[-1] == 0:
            darkL = np.hstack((darkL, self.shutter.size))
        else:
            darkR = np.hstack((darkR, self.shutter.size))

        self.spectra_dark_corr = self.spectra.copy() + countOffset
        self.dark_offset       = np.zeros(self.spectra.shape, dtype=np.float64)
        self.dark_std          = np.zeros(self.spectra.shape, dtype=np.float64)

        Nrecord, Nchannel, Nsensor = self.spectra.shape
        if mode == -1:
            if darkL.size-darkR.size==0:
                if darkL[0]>darkR[0] and darkL[-1]>darkR[-1]:
                    darkL = darkL[:-1]
                    darkR = darkR[1:]
            elif darkL.size-darkR.size==1:
                if darkL[0]>darkR[0] and darkL[-1]<darkR[-1]:
                    darkL = darkL[1:]
                elif darkL[0]<darkR[0] and darkL[-1]>darkR[-1]:
                    darkL = darkL[:-1]
            elif darkR.size-darkL.size==1:
                if darkL[0]>darkR[0] and darkL[-1]<darkR[-1]:
                    darkR = darkR[1:]
                elif darkL[0]<darkR[0] and darkL[-1]>darkR[-1]:
                    darkR = darkR[:-1]
            else:
                exit('Error [READ_SKS.DARK_CORR]: darkL and darkR are wrong.')

            for i in range(darkL.size-1):
                if darkR[i] < darkL[i]:
                    exit('Error [READ_SKS.DARK_CORR]: darkL > darkR.')

                darkLL = darkL[i] + darkExtend
                darkLR = darkR[i] - darkExtend
                darkRL = darkL[i+1] + darkExtend
                darkRR = darkR[i+1] - darkExtend

                if i == 0:
                    self.shutter[:darkLL] = -1  # omit the data before the first dark cycle

                lightL = darkR[i]   + lightExtend
                lightR = darkL[i+1] - lightExtend

                if lightR-lightL>lightThr and darkLR-darkLL>darkThr and darkRR-darkRL>darkThr:

                    self.shutter[darkL[i]:darkLL] = -1
                    self.shutter[darkLR:darkR[i]] = -1
                    self.shutter[darkR[i]:lightL] = -1
                    self.shutter[lightR:darkL[i+1]] = -1
                    self.shutter[darkL[i+1]:darkRL] = -1
                    self.shutter[darkRR:darkR[i+1]] = -1

                    int_dark  = np.append(self.int_time[darkLL:darkLR], self.int_time[darkRL:darkRR]).mean()
                    int_light = self.int_time[lightL:lightR].mean()

                    if np.abs(int_dark - int_light) > 0.0001:
                        self.shutter[lightL:lightR] = -1
                    else:
                        interp_x  = np.append(self.tmhr[darkLL:darkLR], self.tmhr[darkRL:darkRR])
                        if i==darkL.size-2:
                            target_x  = self.tmhr[darkLL:darkRR]
                        else:
                            target_x  = self.tmhr[darkLL:darkRL]

                        for ichan in range(Nchannel):
                            for isen in range(Nsensor):
                                interp_y = np.append(self.spectra[darkLL:darkLR,ichan,isen], self.spectra[darkRL:darkRR,ichan,isen])
                                slope, intercept, r_value, p_value, std_err  = stats.linregress(interp_x, interp_y)
                                if i==darkL.size-2:
                                    self.dark_offset[darkLL:darkRR, ichan, isen] = target_x*slope + intercept
                                    self.spectra_dark_corr[darkLL:darkRR, ichan, isen] -= self.dark_offset[darkLL:darkRR, ichan, isen]
                                    self.dark_std[darkLL:darkRR, ichan, isen] = np.std(interp_y)
                                else:
                                    self.dark_offset[darkLL:darkRL, ichan, isen] = target_x*slope + intercept
                                    self.spectra_dark_corr[darkLL:darkRL, ichan, isen] -= self.dark_offset[darkLL:darkRL, ichan, isen]
                                    self.dark_std[darkLL:darkRL, ichan, isen] = np.std(interp_y)

                else:
                    self.shutter[darkL[i]:darkR[i+1]] = -1

            self.shutter[darkRR:] = -1  # omit the data after the last dark cycle

        elif mode == -2:
            print('Message [DARK_CORR]: Not implemented...')

        elif mode == -3:

            #if darkL.size-darkR.size==0:
                #if darkL[0]>darkR[0] and darkL[-1]>darkR[-1]:
                    #darkL = darkL[:-1]
                    #darkR = darkR[1:]
            #elif darkL.size-darkR.size==1:
                #if darkL[0]>darkR[0] and darkL[-1]<darkR[-1]:
                    #darkL = darkL[1:]
                #elif darkL[0]<darkR[0] and darkL[-1]>darkR[-1]:
                    #darkL = darkL[:-1]
            #elif darkR.size-darkL.size==1:
                #if darkL[0]>darkR[0] and darkL[-1]<darkR[-1]:
                    #darkR = darkR[1:]
                #elif darkL[0]<darkR[0] and darkL[-1]>darkR[-1]:
                    #darkR = darkR[:-1]
            #else:
                #exit('Error [READ_SKS.DARK_CORR]: darkL and darkR are wrong.')

            for i in range(darkR.size):
                darkLL = darkL[i]   + darkExtend
                darkLR = darkR[i]   - darkExtend
                lightL = darkR[i]   + lightExtend
                lightR = darkL[i+1] - lightExtend

                self.shutter[darkL[i]:darkLL] = -1
                self.shutter[darkLR:darkR[i]] = -1
                self.shutter[darkR[i]:lightL] = -1
                self.shutter[lightR:darkL[i+1]] = -1

                int_dark  = self.int_time[darkLL:darkLR].mean()
                int_light = self.int_time[lightL:lightR].mean()
                if np.abs(int_dark - int_light) > 0.0001:
                    self.shutter[lightL:lightR] = -1
                    exit('Error [READ_SKS.DARK_CORR]: inconsistent integration time.')
                else:
                    for itmhr in range(darkLR, lightR):
                        for isen in range(Nsensor):
                            dark_offset0 = np.mean(self.spectra[darkLL:darkLR, :, isen], axis=0)
                            self.dark_offset[itmhr, :, isen] = dark_offset0
                    self.spectra_dark_corr[lightL:lightR,:,:] -= self.dark_offset[lightL:lightR,:,:]

        elif mode == -4:
            print('Message [DARK_CORR]: Not implemented...')

    def NLIN_CORR(self, fname_nlin, Nsen, verbose=False):

        int_time0 = np.mean(self.int_time[:, Nsen])
        f_nlin = readsav(fname_nlin)

        if abs(f_nlin.iin_-int_time0)>1.0e-5:
            exit('Error [READ_SKS]: Integration time do not match.')

        for iwvl in range(256):
            xx0   = self.spectra_nlin_corr[:,iwvl,Nsen].copy()
            xx    = np.zeros_like(xx0)
            yy    = np.zeros_like(xx0)
            #self.spectra_nlin_corr[:,iwvl,Nsen] = np.nan
            self.spectra_nlin_corr[:,iwvl,Nsen] = -1.0
            logic_xx     = (xx0>-100)
            if verbose:
                print('++++++++++++++++++++++++++++++++++++++++++++++++')
                print('range', f_nlin.mxm_[0,iwvl]*f_nlin.in_[iwvl], f_nlin.mxm_[1,iwvl]*f_nlin.in_[iwvl])
                print('good', logic_xx.sum(), xx0.size)
            xx0[logic_xx] = xx0[logic_xx]/f_nlin.in_[iwvl]

            if (f_nlin.bad_[1,iwvl]<1.0) and (f_nlin.mxm_[0,iwvl]>=1.0e-3):

                #+ data in range (0, minimum)
                yy_e = 0.0
                for ideg in range(f_nlin.gr_):
                    yy_e += f_nlin.res2_[ideg,iwvl]*f_nlin.mxm_[0,iwvl]**ideg
                slope = yy_e/f_nlin.mxm_[0,iwvl]
                logic_xx     = (xx0>-100) & (xx0<f_nlin.mxm_[0,iwvl])
                if verbose:
                    print('0-min', logic_xx.sum(), xx0.size)
                    print('data', xx0[logic_xx])
                xx[xx<0]     = 0.0
                xx[logic_xx] = xx0[logic_xx]
                yy[logic_xx] = xx[logic_xx]*slope

                self.spectra_nlin_corr[logic_xx,iwvl,Nsen] = yy[logic_xx]*f_nlin.in_[iwvl]
                #-

                #+ data in range [minimum, maximum]
                logic_xx     = (xx0>=f_nlin.mxm_[0,iwvl]) & (xx0<=f_nlin.mxm_[1,iwvl])
                xx[logic_xx] = xx0[logic_xx]
                if verbose:
                    print('min-max', logic_xx.sum(), xx0.size)
                    print('------------------------------------------------')
                for ideg in range(f_nlin.gr_):
                    yy[logic_xx] += f_nlin.res2_[ideg, iwvl]*xx[logic_xx]**ideg

                self.spectra_nlin_corr[logic_xx,iwvl,Nsen] = yy[logic_xx]*f_nlin.in_[iwvl]
                #-

    def COS_CORR(self, data_plt,
                 fname_zen = 'data/ssfr/aux/20150221_LCx_zenith.out',
                 fname_nad = 'data/ssfr/aux/20150221_LCx_nadir.out'):

        iza, iaa = PRH2ZA(data_plt.ang_pit_a-data_plt.ang_pit_m, data_plt.ang_rol_a-data_plt.ang_rol_m, data_plt.ang_head)
        # iza, iaa = PRH2ZA(data_plt.ang_pit_a, data_plt.ang_rol_a, data_plt.ang_head)

        tmhrMin = self.tmhr_corr.min()
        tmhrMax = self.tmhr_corr.max()

        if data_plt.tmhr_corr.min() > tmhrMin or data_plt.tmhr_corr.max() < tmhrMax:
            exit('Error [READ_SKS.COS_CORR]: GPS time range from INS cannot include SSFR time range to be interpolated.')

        logic = (data_plt.tmhr_corr>=tmhrMin) & (data_plt.tmhr_corr<=tmhrMax)

        # fig = plt.figure(figsize=(8, 6))
        # ax1 = fig.add_subplot(111)
        # plt.scatter(data_plt.tmhr_corr[logic], iaa[logic], s=0.1)
        # ax1.set_xlim((25.0, 25.6))
        # ax1.set_ylim((-5.0, 5.0))
        # # ax1.legend(loc='best', fontsize=12, framealpha=0.4)
        # # plt.savefig('test.png')
        # plt.show()
        pit_a0  = np.interp(self.tmhr_corr, data_plt.tmhr_corr[logic], data_plt.ang_pit_a[logic])
        rol_a0  = np.interp(self.tmhr_corr, data_plt.tmhr_corr[logic], data_plt.ang_rol_a[logic])
        hed_a0  = np.interp(self.tmhr_corr, data_plt.tmhr_corr[logic], data_plt.ang_head[logic])

        # figure settings
        # fig = plt.figure(figsize=(8, 6))
        # ax1 = fig.add_subplot(111)
        # ax1.scatter(self.tmhr_corr, hed_a0, s=2)

        # ax1.legend(loc='best', fontsize=12, framealpha=0.4)
        # plt.savefig('test.png')
        # plt.show()

        pit_m0  = np.interp(self.tmhr_corr, data_plt.tmhr_corr[logic], data_plt.ang_pit_m[logic])
        rol_m0  = np.interp(self.tmhr_corr, data_plt.tmhr_corr[logic], data_plt.ang_rol_m[logic])

        iza0, iaa0 = PRH2ZA(pit_a0-pit_m0, rol_a0-rol_m0, hed_a0)

        lon0  = np.interp(self.tmhr_corr, data_plt.tmhr_corr[logic], data_plt.lon[logic])
        lat0  = np.interp(self.tmhr_corr, data_plt.tmhr_corr[logic], data_plt.lat[logic])
        alt0  = np.interp(self.tmhr_corr, data_plt.tmhr_corr[logic], data_plt.alt[logic])

        # calculate solar zenith angle (sza), solar azimuth angle (saa)
        sza0 = np.zeros_like(self.tmhr_corr)
        saa0 = np.zeros_like(self.tmhr_corr)
        for i in range(lon0.size):
            dtime_i  = self.dateRef + datetime.timedelta(hours=self.tmhr_corr[i])
            dtime_i  = dtime_i.replace(tzinfo=datetime.timezone.utc)
            sza0[i] = 90.0 - pysolar.solar.get_altitude(lat0[i], lon0[i], dtime_i, elevation=alt0[i])
            saa_i = pysolar.solar.get_azimuth(lat0[i], lon0[i], dtime_i, elevation=alt0[i])
            if saa_i >= 0.0:
                if 0.0<=saa_i<=180.0:
                    saa_i = 180.0 - saa_i
                elif 180.0<saa_i<=360.0:
                    saa_i = 540.0 - saa_i
                else:
                    print('Warning [READ_SKS.COS_CORR]: invalid solar azimuth angle.')
            elif saa_i < 0.0:
                if -180.0<=saa_i<0.0:
                    saa_i = -saa_i + 180.0
                elif -360.0<=saa_i<-180.0:
                    saa_i = -saa_i - 180.0
                else:
                    print('Warning [READ_SKS.COS_CORR]: invalid solar azimuth angle.')
            saa0[i] = saa_i

        self.sza0 = sza0
        self.saa0 = saa0
        self.alt0 = alt0

        self.rol_filter = (np.abs(rol_a0-rol_m0)<=0.2)

        dc       = MUSLOPE(sza0, saa0, iza0, iaa0)
        self.dc  = dc

        mu       = np.cos(np.deg2rad(sza0))
        factor   = mu/dc

        indice = np.int_(np.round(dc, decimals=3)*1000.0)
        indice[indice>1000]=1000
        indice[indice<0]   =0

        f_cos = readsav(fname_zen)
        wvl   = f_cos.wvlz
        resp  = f_cos.res_z_wvl
        mu_i  = f_cos.mu_interpol

        wvl0   = 666.0
        index0 = np.argmin(np.abs(wvl-wvl0))
        resp0     = resp[index0, :] - 0.008
        resp0_ori = resp[index0, :]
        resp0[resp0<0.0000001] = 0.0000001
        resp0_ori[resp0_ori<0.0000001] = 0.0000001

        self.cos_corr_factor     = np.cos(np.deg2rad(sza0)) / resp0[indice]
        self.cos_corr_factor_ori = np.cos(np.deg2rad(sza0)) / resp0_ori[indice]

    def COUNT2FLUX(self, countIn,
                   fname_ns = 'data/ssfr/aux/20140924_s1n_150B_s300.sav',
                   fname_ni = 'data/ssfr/aux/20140924_s1n_150B_i300.sav',
                   fname_zs = 'data/ssfr/aux/20140921_s1z_150B_s300.sav',
                   fname_zi = 'data/ssfr/aux/20140921_s1z_150B_i300.sav'
            ):
        """
        Convert digital count to flux (irradiance)
        """

        f_ns = readsav(fname_ns)
        f_ni = readsav(fname_ni)
        f_zs = readsav(fname_zs)
        f_zi = readsav(fname_zi)

        logic_nsi2 = (f_ns.wl_si2 <= f_ni.join)
        logic_nin2 = (f_ni.wl_in2 >= f_ni.join)
        nsi2 = logic_nsi2.sum()
        nin2 = logic_nin2.sum()
        n2 = nsi2 + nin2

        logic_nsi1 = (f_zs.wl_si1 <= f_ni.join)
        logic_nin1 = (f_zi.wl_in1 >= f_ni.join)
        nsi1 = logic_nsi1.sum()
        nin1 = logic_nin1.sum()
        n1 = nsi1 + nin1

        f_ns.resp2_si2[f_ns.resp2_si2<1.0e-10] = -1
        f_ni.resp2_in2[f_ni.resp2_in2<1.0e-10] = -1
        f_zs.resp2_si1[f_zs.resp2_si1<1.0e-10] = -1
        f_zi.resp2_in1[f_zi.resp2_in1<1.0e-10] = -1

        self.wvl_zen = np.append(f_zs.wl_si1[logic_nsi1], f_zi.wl_in1[logic_nin1][::-1])
        self.wvl_nad = np.append(f_ns.wl_si2[logic_nsi2], f_ni.wl_in2[logic_nin2][::-1])
        self.spectra_flux_zen = np.zeros((self.tmhr_corr.size, n1), dtype=np.float64)
        self.spectra_flux_nad = np.zeros((self.tmhr_corr.size, n2), dtype=np.float64)
        for i in range(self.tmhr_corr.size):
            if self.shutter[i] == 0:
                self.spectra_flux_zen[i, :nsi1] = countIn[i, logic_nsi1, 0]/float(self.int_time[i, 0])/f_zs.resp2_si1[logic_nsi1]
                self.spectra_flux_zen[i, nsi1:] = (countIn[i, logic_nin1, 1]/float(self.int_time[i, 1])/f_zi.resp2_in1[logic_nin1])[::-1]
                self.spectra_flux_nad[i, :nsi2] = countIn[i, logic_nsi2, 2]/float(self.int_time[i, 2])/f_ns.resp2_si2[logic_nsi2]
                self.spectra_flux_nad[i, nsi2:] = (countIn[i, logic_nin2, 3]/float(self.int_time[i, 3])/f_ni.resp2_in2[logic_nin2])[::-1]
            else:
                self.spectra_flux_zen[i, :] = -1.0
                self.spectra_flux_nad[i, :] = -1.0

        self.spectra_flux_zen[self.spectra_flux_zen<0.0] = np.nan
        self.spectra_flux_nad[self.spectra_flux_nad<0.0] = np.nan
# ----------------------------------------------------------------------------

# ++++++++++++++++++++++++    for .plt2 files   ++++++++++++++++++++++++++++++
def PRH2ZA(ang_pit, ang_rol, ang_head, is_rad=False):

    """
    input:
    ang_pit   (Pitch)   [deg]: positive (+) values indicate nose up
    ang_rol   (Roll)    [deg]: positive (+) values indicate right wing down
    ang_head  (Heading) [deg]: positive (+) values clockwise, w.r.t. north

    "vec": normal vector of the surface of the sensor

    return:
    ang_zenith : angle of "vec" [deg]
    ang_azimuth: angle of "vec" [deg]: positive (+) values clockwise, w.r.t. north
    """

    if not is_rad:
        rad_pit  = np.deg2rad(ang_pit)
        rad_rol  = np.deg2rad(ang_rol)
        rad_head = np.deg2rad(ang_head)

    uz =  np.cos(rad_rol)*np.cos(rad_pit)
    ux =  np.sin(rad_rol)
    uy = -np.cos(rad_rol)*np.sin(rad_pit)

    vz = uz
    vx = ux*np.cos(rad_head) + uy*np.sin(rad_head)
    vy = uy*np.cos(rad_head) - ux*np.sin(rad_head)

    ang_zenith  = np.rad2deg(np.arccos(vz))
    ang_azimuth = np.rad2deg(np.arctan2(vx,vy))

    return ang_zenith, ang_azimuth

def MUSLOPE(sza, saa, iza, iaa, is_rad=False):

    if not is_rad:
        rad_sza = np.deg2rad(sza)
        rad_saa = np.deg2rad(saa)
        rad_iza = np.deg2rad(iza)
        rad_iaa = np.deg2rad(iaa)

    zs = np.cos(rad_sza)
    ys = np.sin(rad_sza) * np.cos(rad_saa)
    xs = np.sin(rad_sza) * np.sin(rad_saa)

    zi = np.cos(rad_iza)
    yi = np.sin(rad_iza) * np.cos(rad_iaa)
    xi = np.sin(rad_iza) * np.sin(rad_iaa)

    mu = xs*xi + ys*yi + zs*zi

    return mu

def READ_PLT2_ONE_V1(fname, vnames=None, dataLen=126, verbose=False):

    fileSize = os.path.getsize(fname)
    if fileSize > dataLen:
        iterN    = fileSize // dataLen
        residual = fileSize%  dataLen
        if residual != 0:
            print('Warning [READ_PLT2_V2]: %s has invalid data size.' % fname)
    else:
        exit('Error [READ_PLT2_V2]: %s has invalid file size.' % fname)

    vnames_dict  = {
                   'mini_pit':0,
                   'mini_rol':1,
                 'GPSSeconds':2,
                    'GPSWeek':3,
                        'Lat':4,
                        'Lon':5,
                        'Alt':6,
                        'Rol':7,
                        'Pit':8,
                        'Azi':9,
             'North_velocity':10,
              'East_velocity':11,
                'Up_velocity':12,
                  'motor_Rol':13,
                  'motor_Pit':14,
                 'temp_stage':15,
               'temp_c_pitch':16,
                 'temp_c_rol':17,
               'temp_m_pitch':18,
                 'temp_m_rol':19,
                      'acc_x':20,
                      'acc_y':21,
                      'acc_z':22,
          'relative_humidity':23,
        'chassis_temperature':24,
             'system_voltage':25,
      'INS_solution_inactive':26,
               'INS_aligning':27,
      'INS_solution_not_good':28,
          'INS_solution_good':29,
      'INS_bad_GPS_agreement':30,
     'INS_alignment_complete':31,
               'power_status':32,
            'SPAN_CPT_status':33,
                'file_status':34,
               'pitch_status':35,
                'roll_status':36,
                'FPGA_status':37,
                  'ARINC_rol':38,
                  'ARINC_pit':39,
                  'data_flag':40,
            'mini_INS_status':41
                  }

    if vnames == None:
        dataAll = np.zeros((iterN, len(vnames_dict)), dtype=np.float64)
        print('Warning [READ_PLT2_V2]: some variables should be integer type, please change accordingly.')
    else:
        dataAll = np.zeros((iterN, len(vnames)), dtype=np.float64)
        vnames_indice = []
        for vname in vnames:
            if vnames_dict[vname] in [26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 40, 41]:
                print('Warning [READ_PLT2_V2]: %s should be integer type, please change accordingly.' % vname)
            vnames_indice.append(vnames_dict[vname])

    f = open(fname, 'rb')
    if verbose:
        print('++++++++++++++++++++++++++++++++++++++++++++++++++')
        print('Reading %s...' % fname.split('/')[-1])

    # read data record
    for i in range(iterN):
        dataRec = f.read(dataLen)
        data    = struct.unpack('<26f12B2f2B', dataRec)
        if vnames == None:
            dataAll[i,:] = np.array(data, dtype=np.float64)
        else:
            dataAll[i,:] = np.array(data, dtype=np.float64)[vnames_indice]

    if verbose:
        print(dataAll[:, 0].min()/86400.0, dataAll[:, 0].max()/86400.0)
        print('--------------------------------------------------')

    return dataAll

class READ_PLT2:
    def __init__(self, fnames, dateRef=None, Ndata=15000, config=None, secOffset=0.0):

        if type(fnames) is not list:
            exit('Error [READ_PLT2]: input variable "fnames" should be in list type.')

        vnames=['GPSSeconds', 'GPSWeek', 'Pit', 'Rol', 'motor_Pit', 'motor_Rol', 'Azi', 'Lon', 'Lat', 'Alt', 'ARINC_pit', 'ARINC_rol']
        Nx         = Ndata * len(fnames)
        dataAll    = np.zeros((Nx, len(vnames)), dtype=np.float64)

        Nstart = 0
        for fname in fnames:
            dataAll0 = READ_PLT2_ONE_V1(fname, vnames=vnames, dataLen=126, verbose=False)
            Nend = Nstart + dataAll0.shape[0]
            dataAll[Nstart:Nend, ...]  = dataAll0
            Nstart = Nend

        if config != None:
            self.config = config

        dataAll   = dataAll[:Nend, ...]

        date0   = datetime.datetime(1980, 1, 6)
        jday0   = (date0   -(datetime.datetime(1, 1, 1))).days + 1.0

        self.jday = dataAll[:, 0]/86400.0 + 7.0* dataAll[:, 1] + jday0
        if dateRef == None:
            jdayRef = int(self.jday[0])
        else:
            jdayRef = (dateRef -(datetime.datetime(1, 1, 1))).days + 1.0

        self.tmhr = (self.jday-jdayRef)*24.0

        self.secOffset = secOffset
        self.jday_corr = self.jday - secOffset/86400.0
        self.tmhr_corr = self.tmhr - secOffset/3600.0

        self.ang_pit     = dataAll[:, 2] # light collector pitch/roll angle
        self.ang_rol     = dataAll[:, 3]
        self.ang_pit_m   = dataAll[:, 4] # motor pitch/roll angle
        self.ang_rol_m   = dataAll[:, 5]
        self.ang_head360 = dataAll[:, 6] # heading angle
        self.lon         = dataAll[:, 7]
        self.lat         = dataAll[:, 8]
        self.alt         = dataAll[:, 9]
        self.ang_pit_a   = dataAll[:, 10]# aircraft pitch/roll angle
        self.ang_rol_a   = dataAll[:, 11]

        self.ang_head    = self.ang_head360.copy()
        self.ang_head[self.ang_head>180.0] -= 360.0

def READ_PLT3_ONE_V1(fname, vnames=None, dataLen=126, verbose=False):

    fileSize = os.path.getsize(fname)
    if fileSize > dataLen:
        iterN    = fileSize // dataLen
        residual = fileSize%  dataLen
        if residual != 0:
            print('Warning [READ_PLT2_V2]: %s has invalid data size.' % fname)
    else:
        exit('Error [READ_PLT2_V2]: %s has invalid file size.' % fname)

    vnames_dict  = {
                   'mini_pit':0,
                   'mini_rol':1,
                 'GPSSeconds':2,
                    'GPSWeek':3,
                        'Lat':4,
                        'Lon':5,
                        'Alt':6,
                        'Rol':7,
                        'Pit':8,
                        'Azi':9,
             'North_velocity':10,
              'East_velocity':11,
                'Up_velocity':12,
                  'motor_Rol':13,
                  'motor_Pit':14,
                 'temp_stage':15,
               'temp_c_pitch':16,
                 'temp_c_rol':17,
               'temp_m_pitch':18,
                 'temp_m_rol':19,
                      'acc_x':20,
                      'acc_y':21,
                      'acc_z':22,
          'relative_humidity':23,
        'chassis_temperature':24,
             'system_voltage':25,
      'INS_solution_inactive':26,
               'INS_aligning':27,
      'INS_solution_not_good':28,
          'INS_solution_good':29,
      'INS_bad_GPS_agreement':30,
     'INS_alignment_complete':31,
               'power_status':32,
            'SPAN_CPT_status':33,
                'file_status':34,
               'pitch_status':35,
                'roll_status':36,
                'FPGA_status':37,
                  'ARINC_rol':38,
                  'ARINC_pit':39,
                  'data_flag':40,
            'mini_INS_status':41
                  }

    if vnames == None:
        dataAll = np.zeros((iterN, len(vnames_dict)), dtype=np.float64)
        print('Warning [READ_PLT2_V2]: some variables should be integer type, please change accordingly.')
    else:
        dataAll = np.zeros((iterN, len(vnames)), dtype=np.float64)
        vnames_indice = []
        for vname in vnames:
            if vnames_dict[vname] in [26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 40, 41]:
                print('Warning [READ_PLT2_V2]: %s should be integer type, please change accordingly.' % vname)
            vnames_indice.append(vnames_dict[vname])

    f = open(fname, 'rb')
    if verbose:
        print('++++++++++++++++++++++++++++++++++++++++++++++++++')
        print('Reading %s...' % fname.split('/')[-1])

    # read data record
    for i in range(iterN):
        dataRec = f.read(dataLen)
        data    = struct.unpack('<26f12B2f2B', dataRec)
        if vnames == None:
            dataAll[i,:] = np.array(data, dtype=np.float64)
        else:
            dataAll[i,:] = np.array(data, dtype=np.float64)[vnames_indice]

    if verbose:
        print(dataAll[:, 0].min()/86400.0, dataAll[:, 0].max()/86400.0)
        print('--------------------------------------------------')

    return dataAll

class READ_PLT3:
    def __init__(self, fnames, dateRef=None, Ndata=15000, config=None, secOffset=0.0):

        if type(fnames) is not list:
            exit('Error [READ_PLT2]: input variable "fnames" should be in list type.')

        vnames=['GPSSeconds', 'GPSWeek', 'Pit', 'Rol', 'motor_Pit', 'motor_Rol', 'Azi', 'Lon', 'Lat', 'Alt', 'ARINC_pit', 'ARINC_rol']
        Nx         = Ndata * len(fnames)
        dataAll    = np.zeros((Nx, len(vnames)), dtype=np.float64)

        Nstart = 0
        for fname in fnames:
            dataAll0 = READ_PLT2_ONE_V1(fname, vnames=vnames, dataLen=126, verbose=False)
            Nend = Nstart + dataAll0.shape[0]
            dataAll[Nstart:Nend, ...]  = dataAll0
            Nstart = Nend

        if config != None:
            self.config = config

        dataAll   = dataAll[:Nend, ...]

        date0   = datetime.datetime(1980, 1, 6)
        jday0   = (date0   -(datetime.datetime(1, 1, 1))).days + 1.0

        self.jday = dataAll[:, 0]/86400.0 + 7.0* dataAll[:, 1] + jday0
        if dateRef == None:
            jdayRef = int(self.jday[0])
        else:
            jdayRef = (dateRef -(datetime.datetime(1, 1, 1))).days + 1.0

        self.tmhr = (self.jday-jdayRef)*24.0

        self.secOffset = secOffset
        self.jday_corr = self.jday - secOffset/86400.0
        self.tmhr_corr = self.tmhr - secOffset/3600.0

        self.ang_pit     = dataAll[:, 2] # light collector pitch/roll angle
        self.ang_rol     = dataAll[:, 3]
        self.ang_pit_m   = dataAll[:, 4] # motor pitch/roll angle
        self.ang_rol_m   = dataAll[:, 5]
        self.ang_head360 = dataAll[:, 6] # heading angle
        self.lon         = dataAll[:, 7]
        self.lat         = dataAll[:, 8]
        self.alt         = dataAll[:, 9]
        self.ang_pit_a   = dataAll[:, 10]# aircraft pitch/roll angle
        self.ang_rol_a   = dataAll[:, 11]

        self.ang_head    = self.ang_head360.copy()
        self.ang_head[self.ang_head>180.0] -= 360.0
# ----------------------------------------------------------------------------

def CDATA_NLIN(dist,
        wvl_ref=480,
        tag='nadir',
        fname_std='/argus/field/arise/cal/506C_NIST_resample_bulb'):

    NChan = 256
    NFile = dist.size

    xxChan = np.arange(NChan)

    if tag == 'zenith':
        coef_si = np.array([301.946,  3.31877,  0.00037585,  -1.76779e-6, 0])
        coef_in = np.array([2202.33, -4.35275, -0.00269498,   3.84968e-6, -2.33845e-8])
        iSi     = 0
        iIn     = 1
    elif tag == 'nadir':
        coef_si = np.array([302.818,  3.31912,  0.000343831, -1.81135e-6, 0])
        coef_in = np.array([2210.29,  -4.5998,  0.00102444,  -1.60349e-5, 1.29122e-8])
        iSi     = 2
        iIn     = 3

    wvl_si  = coef_si[0] + coef_si[1]*xxChan + coef_si[2]*xxChan**2 + coef_si[3]*xxChan**3 + coef_si[4]*xxChan**4
    wvl_in  = coef_in[0] + coef_in[1]*xxChan + coef_in[2]*xxChan**2 + coef_in[3]*xxChan**3 + coef_in[4]*xxChan**4

    fnames_sks = sorted(glob.glob('/argus/home/chen/work/02_arise/06_cal/data/lin_data/20161009_Field_A_150_150*.SKS'))
    #fnames_sks = sorted(glob.glob('/argus/home/chen/work/02_arise/06_cal/data/lin_data/20161009_Field_A_200_200*.SKS'))
    #fnames_sks = sorted(glob.glob('/argus/home/chen/work/02_arise/06_cal/data/lin_data/20161009_Field_A_250_250*.SKS'))
    if len(fnames_sks) != NFile:
        exit('Error [CDATA_NLIN]: the number of input .sks files are inconsistent with NFile.')

    intTime_si  = np.zeros( NFile,            dtype=np.float64)
    intTime_in  = np.zeros( NFile,            dtype=np.float64)
    data2fit_si = np.zeros((NFile, NChan, 2), dtype=np.float64) # index 0: mean, index 1: standard deviation
    data2fit_in = np.zeros((NFile, NChan, 2), dtype=np.float64)
    dark_offset_si = np.zeros((NFile, NChan), dtype=np.float64)
    dark_offset_in = np.zeros((NFile, NChan), dtype=np.float64)

    for i in range(NFile):
        fname_sks = fnames_sks[i]
        data_sks  = READ_SKS([fname_sks])
        logic = (data_sks.shutter==0)

        intTime_si[i]        = np.mean(data_sks.int_time[logic, iSi])
        intTime_in[i]        = np.mean(data_sks.int_time[logic, iIn])
        data2fit_si[i, :, 0] = np.mean(data_sks.spectra_dark_corr[logic, :, iSi], axis=0)
        data2fit_si[i, :, 1] = np.std( data_sks.spectra_dark_corr[logic, :, iSi], axis=0)
        data2fit_in[i, :, 0] = np.mean(data_sks.spectra_dark_corr[logic, :, iIn], axis=0)
        data2fit_in[i, :, 1] = np.std( data_sks.spectra_dark_corr[logic, :, iIn], axis=0)

        dark_offset_si[i, :]    = np.mean(data_sks.dark_offset[logic, :, iSi], axis=0)
        dark_offset_in[i, :]    = np.mean(data_sks.dark_offset[logic, :, iIn], axis=0)

    # read standard (50cm) lamp file
    # data_std[:, 0]: wavelength in [nm]
    # data_std[:, 1]: irradiance in [microW cm^-2 nm^-1]
    data_std = np.loadtxt(fname_std)
    resp_si_interp = np.interp(wvl_si, data_std[:, 0], data_std[:, 1]*0.01)
    resp_in_interp = np.interp(wvl_in, data_std[:, 0], data_std[:, 1]*0.01)

    indexCal = 0
    resp_si = None
    resp_in = None
    if resp_si is None and resp_in is None:
        std_si  = data2fit_si[indexCal, :, 0]
        std_in  = data2fit_in[indexCal, :, 0]
        resp_si = std_si / intTime_si[indexCal] / resp_si_interp
        resp_in = std_in / intTime_in[indexCal] / resp_in_interp

    plt_logic = True
    if plt_logic:
        rcParams['font.size'] = 12
        rcParams['xtick.direction'] = 'in'
        rcParams['ytick.direction'] = 'in'
        fig = plt.figure(figsize=(16, 5))
        gs  = gridspec.GridSpec(1, 3)
        ax1 = plt.subplot(gs[0, 0])
        ax2 = plt.subplot(gs[0, 1])
        ax3 = plt.subplot(gs[0, 2])
        ax1.plot(wvl_si, data2fit_si[indexCal, :, 0], color='k')
        ax1.axvline(wvl_ref, color='grey', ls='--')
        ax1.axhline(0, color='k', ls=':')
        ax1.set_title('Silicon')
        ax1.set_xlabel('Wavelength [nm]')
        ax1.set_ylabel('Dark Corrected Counts')
        ax1.set_xlim([200, 1200])
        ax1.set_ylim([0, 18000])
        ax1.ticklabel_format(style='sci',axis='y', scilimits=(-3,4))

        ax2.plot(wvl_in, data2fit_in[indexCal, :, 0], color='k')
        ax2.axhline(0, color='k', ls=':')
        ax2.set_title('InGaAs')
        ax2.set_xlabel('Wavelength [nm]')
        ax2.set_ylabel('Dark Corrected Counts')
        ax2.set_xlim([500, 2500])
        ax2.ticklabel_format(style='sci',axis='y', scilimits=(-3,4))
        ax2.set_ylim([0, 25000])

        ax3.plot(data_std[:, 0], data_std[:, 1]*0.01, label='Lamp', zorder=0)
        ax3.plot(wvl_si, data2fit_si[indexCal, :, 0]/intTime_si[indexCal]/resp_si, color='red', alpha=0.4, zorder=1)
        ax3.fill_between(wvl_si, (data2fit_si[indexCal, :, 0]-data2fit_si[indexCal, :, 1])/intTime_si[indexCal]/resp_si, (data2fit_si[indexCal, :, 0]+data2fit_si[indexCal, :, 1])/intTime_si[indexCal]/resp_si, facecolor='r', alpha=0.4, lw=0, label='Cal Si', zorder=1)

        ax3.plot(wvl_in, data2fit_in[indexCal, :, 0]/intTime_in[indexCal]/resp_in, color='blue', alpha=0.4, zorder=1)
        ax3.fill_between(wvl_in, (data2fit_in[indexCal, :, 0]-data2fit_in[indexCal, :, 1])/intTime_in[indexCal]/resp_in, (data2fit_in[indexCal, :, 0]+data2fit_in[indexCal, :, 1])/intTime_in[indexCal]/resp_in, facecolor='b', alpha=0.4, lw=0, label='Cal In', zorder=1)
        ax3.axhline(0, color='k', ls=':')
        ax3.set_title('Lamp Spectrum')
        ax3.set_xlabel('Wavelength [nm]')
        ax3.set_ylabel('Irradiance [$\mathrm{W m^2 nm^{-1}}$]')
        ax3.set_xlim([350, 2200])
        ax3.set_ylim([0, 0.3])
        ax3.legend(loc='upper right', fontsize=11, framealpha=0.2)
        plt.savefig('fig0.png')
        #plt.show()
    #exit()

    plt_logic = True
    if plt_logic:
        for indexCal in range(NFile):
            rcParams['font.size'] = 12
            rcParams['xtick.direction'] = 'in'
            rcParams['ytick.direction'] = 'in'
            fig = plt.figure(figsize=(16, 5))
            gs  = gridspec.GridSpec(1, 3)
            ax1 = plt.subplot(gs[0, 0])
            ax2 = plt.subplot(gs[0, 1])
            ax3 = plt.subplot(gs[0, 2])

            ax1.plot(wvl_si, data2fit_si[indexCal, :, 0]/intTime_si[indexCal]/resp_si, color='red', alpha=0.6, zorder=1)
            ax1.fill_between(wvl_si, (data2fit_si[indexCal, :, 0]-data2fit_si[indexCal, :, 1])/intTime_si[indexCal]/resp_si, (data2fit_si[indexCal, :, 0]+data2fit_si[indexCal, :, 1])/intTime_si[indexCal]/resp_si, facecolor='r', alpha=0.4, lw=0, label='Cal Si', zorder=1)
            ax1.plot(wvl_in, data2fit_in[indexCal, :, 0]/intTime_in[indexCal]/resp_in, color='blue', alpha=0.6, zorder=1)
            ax1.fill_between(wvl_in, (data2fit_in[indexCal, :, 0]-data2fit_in[indexCal, :, 1])/intTime_in[indexCal]/resp_in, (data2fit_in[indexCal, :, 0]+data2fit_in[indexCal, :, 1])/intTime_in[indexCal]/resp_in, facecolor='b', alpha=0.4, lw=0, label='Cal In', zorder=1)
            ax1.axvline(wvl_ref, color='grey', ls='--')
            ax1.set_title('Spectrum (Distance=%.1fcm)' % dist[indexCal])
            ax1.set_xlabel('Wavelength [nm]')
            ax1.set_ylabel('Irradiance [$\mathrm{W m^2 nm^{-1}}$]')
            ax1.set_xlim([350, 2200])
            ax1.set_ylim([0, 0.5])

            ax2.plot(wvl_in, data2fit_in[indexCal, :, 0], color='b', alpha=0.6)
            ax2.plot(wvl_in, dark_offset_in[indexCal, :], color='k', alpha=0.6)
            ax2.set_title('InGaAs')
            ax2.set_xlabel('Wavelength [nm]')
            ax2.set_ylabel('Dark Corrected Counts')
            ax2.set_xlim([500, 2500])
            ax2.set_ylim([0, 50000])
            ax2.ticklabel_format(style='sci',axis='y', scilimits=(-3,4))

            ax3.plot(wvl_si, data2fit_si[indexCal, :, 0]/intTime_si[indexCal]/resp_si/resp_si_interp, color='r', alpha=0.6, zorder=1)
            ax3.fill_between(wvl_si, (data2fit_si[indexCal, :, 0]-data2fit_si[indexCal, :, 1])/intTime_si[indexCal]/resp_si/resp_si_interp, (data2fit_si[indexCal, :, 0]+data2fit_si[indexCal, :, 1])/intTime_si[indexCal]/resp_si/resp_si_interp, facecolor='r', alpha=0.4, lw=0, label='Si', zorder=1)
            ax3.plot(wvl_in, data2fit_in[indexCal, :, 0]/intTime_in[indexCal]/resp_in/resp_in_interp, color='b', alpha=0.6, zorder=2)
            ax3.axvline(wvl_ref, color='grey', ls=':')
            ax3.axhline(1.0, color='k', zorder=0)
            ax3.axhline((50.0/dist[indexCal])**2, color='g', ls='--', alpha=0.6)
            ax3.axhline((50.0/dist[indexCal])**2 * 1.03, color='g', ls='--', alpha=0.6, zorder=0)
            ax3.axhline((50.0/dist[indexCal])**2 * 0.97, color='g', ls='--', alpha=0.6, zorder=0)
            #ax3.set_title('Lamp Spectrum')
            ax3.set_xlabel('Wavelength [nm]')
            ax3.set_ylabel('Ratio [Measurement/Lamp]')
            ax3.set_xlim([350, 1600])
            ylim_mid = np.round((50.0/dist[indexCal])**2, decimals=1)
            ax3.set_ylim([ylim_mid-0.4, ylim_mid+0.4])
            plt.savefig('fig1_dist%2.2d.png' % indexCal)
            plt.close(fig)
            #}}}

    poly_degree = 3
    coef_linear = np.zeros((NChan, 2), dtype=np.float64)
    coef_poly   = np.zeros((NChan, poly_degree+1), dtype=np.float64)
    nlin_deg    = np.zeros((NChan, 2), dtype=np.float64)
    x_minmax    = np.zeros((NChan, 2), dtype=np.float64)
    index_wvl_ref = np.argmin(np.abs(wvl_si-wvl_ref))
    for iChan in range(NChan):
        interp_x0 = data2fit_in[:, iChan, 0] / std_in[iChan]
        interp_y0 = data2fit_si[:, index_wvl_ref, 0] / std_si[index_wvl_ref]
        index_sort = np.argsort(interp_x0)
        interp_x = interp_x0[index_sort]
        interp_y = interp_y0[index_sort]

        coef_linear[iChan, 1], coef_linear[iChan, 0], r_value, p_value, std_err = stats.linregress(interp_x, interp_y)
        coef_poly[iChan, ::-1] = np.polyfit(interp_x, interp_y, poly_degree)

        x_minmax[iChan, 0] = np.min(interp_x)
        x_minmax[iChan, 1] = np.max(interp_x)

        lin_y  = coef_linear[iChan, 0] + coef_linear[iChan, 1]*interp_x
        nlin_y = np.zeros_like(lin_y)
        for exp in range(poly_degree+1):
            nlin_y += coef_poly[iChan, exp]*interp_x**exp

        nlin_deg[iChan, 0] = np.max(np.abs(( lin_y/(data2fit_si[:, index_wvl_ref, 0]/std_si[index_wvl_ref])-1.0)*100.0))
        nlin_deg[iChan, 1] = np.max(np.abs((nlin_y/(data2fit_si[:, index_wvl_ref, 0]/std_si[index_wvl_ref])-1.0)*100.0))

    plt_logic = True
    if plt_logic:
        for indexCal in range(NChan):
            #{{{
            rcParams['font.size'] = 12
            rcParams['xtick.direction'] = 'in'
            rcParams['ytick.direction'] = 'in'
            fig = plt.figure(figsize=(11, 5))
            gs  = gridspec.GridSpec(1, 2)
            ax1 = plt.subplot(gs[0, 0])
            ax2 = plt.subplot(gs[0, 1])

            ax1.scatter(data2fit_si[:, index_wvl_ref, 0]/std_si[index_wvl_ref], data2fit_in[:, indexCal, 0]/std_in[indexCal], edgecolor='k', facecolor='none', alpha=0.6, s=60, zorder=1, marker='s')

            xerr = (data2fit_si[:, index_wvl_ref, 1])/std_si[index_wvl_ref]
            yerr = (data2fit_in[:, indexCal     , 1])/std_in[indexCal     ]
            ax1.errorbar(data2fit_si[:, index_wvl_ref, 0]/std_si[index_wvl_ref], data2fit_in[:, indexCal, 0]/std_in[indexCal], xerr=xerr, yerr=yerr, fmt='none')

            ax1.plot([1.0, 1.0], [0.8, 1.2], color='g')
            ax1.plot([0.8, 1.2], [1.0, 1.0], color='g')
            ax1.plot([-10, 10], [-10, 10], color='k', ls=':')

            xx = np.linspace(-10, 10, 1000)
            lin_y    = coef_linear[indexCal, 0] + coef_linear[indexCal, 1]*xx
            nlin_y = np.zeros_like(lin_y)
            for exp in range(poly_degree+1):
                nlin_y += coef_poly[indexCal, exp]*xx**exp
            ax1.plot( lin_y, xx, color='r', alpha=0.6)
            ax1.plot(nlin_y, xx, color='b', alpha=0.6)

            ax1.set_title('Channel %d' % (indexCal+1))
            ax1.set_xlabel('Silicon/Silicon_Cal_50cm')
            ax1.set_ylabel('InGaAs/InGaAs_Cal_50cm')
            ax1.set_xlim([0.5, 2.5])
            ax1.set_ylim([0.5, 2.5])

            ax2.axvline(1.0, color='k', ls='--')
            ax2.axhline( 0.3, color='g', ls=':')
            ax2.axhline(-0.3, color='g', ls=':')
            xx = data2fit_in[:, indexCal, 0] / std_in[indexCal]
            lin_y    = coef_linear[indexCal, 0] + coef_linear[indexCal, 1]*xx
            nlin_y = np.zeros_like(lin_y)
            for exp in range(poly_degree+1):
                nlin_y += coef_poly[indexCal, exp]*xx**exp

            ax2.scatter(xx, (    xx/(data2fit_si[:, index_wvl_ref, 0]/std_si[index_wvl_ref])-1.0)*100.0, c='k', s=50, alpha=0.4, label='residual from 1:1')
            ax2.scatter(xx, ( lin_y/(data2fit_si[:, index_wvl_ref, 0]/std_si[index_wvl_ref])-1.0)*100.0, c='r', s=50, alpha=0.9, label='residual from lin')
            ax2.scatter(xx, (nlin_y/(data2fit_si[:, index_wvl_ref, 0]/std_si[index_wvl_ref])-1.0)*100.0, c='b', s=50, alpha=0.9, label='residual from nlin')

            yy = data2fit_si[:, index_wvl_ref, 0] / std_si[index_wvl_ref]
            ax2.scatter(xx, yy, edgecolor='k', facecolor='none', alpha=0.6, s=60, zorder=1, marker='s')

            xerr = (data2fit_in[:, indexCal, 1]/std_in[indexCal])
            xx = (data2fit_in[:, indexCal, 0]/std_in[indexCal])
            yy = (nlin_y/(data2fit_si[:,index_wvl_ref,0]/std_si[index_wvl_ref])-1.0)*100.0
            ax2.errorbar(xx, yy, xerr=xerr, yerr=0, fmt='none')

            ax2.set_xlim([0.5, 2.5])
            ax2.legend(loc='lower right', fontsize=8, framealpha=0.3)
            ax2.set_xlabel('InGaAs/InGaAs_Cal_50cm')
            ax2.set_ylabel('Non-linearity Degree [%]')
            ax2.set_title('Wavelength %.2f nm' % wvl_in[indexCal])
            plt.savefig('fig2_chan%3.3d.png' % (indexCal+1))
            plt.close(fig)
            #plt.show()

    plt_logic = False
    if plt_logic:
        rcParams['font.size'] = 12
        rcParams['xtick.direction'] = 'in'
        rcParams['ytick.direction'] = 'in'
        fig = plt.figure(figsize=(11, 5))
        ax1 = fig.add_subplot(211)
        ax2 = fig.add_subplot(212)

        ax1.plot(wvl_in, coef_poly[:, -1])
        ax1.axhline(0.0, color='red', lw=0.8, ls=':')
        ax1.set_ylabel('Quadratic Term')
        ax1.set_ylim([-0.1, 0.1])

        #ax2.plot(wvl_in, nlin_deg[:, 1])
        #ax2.set_ylim([162, 164])
        #offset = np.zeros((NFile, NChan), dtype=np.float64)
        #for i in range(NFile):
            #xx = dist[i]
            #for exp in range(poly_degree+1):
                #nlin_y += coef_poly[:, exp]*xx**exp

        #offset = np.array([])
        #for indexCal in range(NChan):
            #xx = data2fit_in[:, indexCal, 0] / std_in[indexCal]
            #nlin_y = np.zeros_like(lin_y)
            #for exp in range(poly_degree+1):
                #nlin_y += coef_poly[indexCal, exp]*xx**exp
            #offset = np.append(offset, nliny-)


        ax2.set_ylabel('Offset')
        ax2.set_xlabel('Wavelength [nm]')
        plt.savefig('quadratic.png')
        #plt.show()

def CDATA_NLIN_cRIO(dist,
        wvl_ref=480,
        tag='nadir',
        fname_std='/argus/field/arise/cal/506C_NIST_resample_bulb'):

    NChan = 256
    NFile = dist.size

    xxChan = np.arange(NChan)

    if tag == 'zenith':
        coef_si = np.array([301.946,  3.31877,  0.00037585,  -1.76779e-6, 0])
        coef_in = np.array([2202.33, -4.35275, -0.00269498,   3.84968e-6, -2.33845e-8])
        iSi     = 0
        iIn     = 1
    elif tag == 'nadir':
        coef_si = np.array([302.818,  3.31912,  0.000343831, -1.81135e-6, 0])
        coef_in = np.array([2210.29,  -4.5998,  0.00102444,  -1.60349e-5, 1.29122e-8])
        iSi     = 2
        iIn     = 3

    wvl_si  = coef_si[0] + coef_si[1]*xxChan + coef_si[2]*xxChan**2 + coef_si[3]*xxChan**3 + coef_si[4]*xxChan**4
    wvl_in  = coef_in[0] + coef_in[1]*xxChan + coef_in[2]*xxChan**2 + coef_in[3]*xxChan**3 + coef_in[4]*xxChan**4

    fnames_sks = sorted(glob.glob('/argus/home/chen/work/02_arise/06_cal/data/lin_data/20161009_Field_A_200_200*.SKS'))

    intTime_si  = np.zeros( NFile,            dtype=np.float64)
    intTime_in  = np.zeros( NFile,            dtype=np.float64)
    data2fit_si = np.zeros((NFile, NChan, 2), dtype=np.float64) # index 0: mean, index 1: standard deviation
    data2fit_in = np.zeros((NFile, NChan, 2), dtype=np.float64)
    dark_offset_si = np.zeros((NFile, NChan), dtype=np.float64)
    dark_offset_in = np.zeros((NFile, NChan), dtype=np.float64)

    for i in range(NFile):
        fname_sks = fnames_sks[i]
        data_sks  = READ_SKS([fname_sks], dateRef=datetime.datetime(2016,10,9))
        logic = (data_sks.shutter==0)

        intTime_si[i]        = np.mean(data_sks.int_time[logic, iSi])
        intTime_in[i]        = np.mean(data_sks.int_time[logic, iIn])
        data2fit_si[i, :, 0] = np.mean(data_sks.spectra_dark_corr[logic, :, iSi], axis=0)
        data2fit_si[i, :, 1] = np.std( data_sks.spectra_dark_corr[logic, :, iSi], axis=0)
        data2fit_in[i, :, 0] = np.mean(data_sks.spectra_dark_corr[logic, :, iIn], axis=0)
        data2fit_in[i, :, 1] = np.std( data_sks.spectra_dark_corr[logic, :, iIn], axis=0)

        dark_offset_si[i, :]    = np.mean(data_sks.dark_offset[logic, :, iSi], axis=0)
        dark_offset_in[i, :]    = np.mean(data_sks.dark_offset[logic, :, iIn], axis=0)

    # read standard (50cm) lamp file
    # data_std[:, 0]: wavelength in [nm]
    # data_std[:, 1]: irradiance in [microW cm^-2 nm^-1]
    data_std = np.loadtxt(fname_std)
    resp_si_interp = np.interp(wvl_si, data_std[:, 0], data_std[:, 1]*0.01)
    resp_in_interp = np.interp(wvl_in, data_std[:, 0], data_std[:, 1]*0.01)

    indexCal = 0
    resp_si = None
    resp_in = None
    if resp_si is None and resp_in is None:
        std_si  = data2fit_si[indexCal, :, 0]
        std_in  = data2fit_in[indexCal, :, 0]
        resp_si = std_si / intTime_si[indexCal] / resp_si_interp
        resp_in = std_in / intTime_in[indexCal] / resp_in_interp

    plt_logic = True
    if plt_logic:
    #if True:
        dist0  = np.unique(dist)

        colors = plt.cm.jet(np.linspace(0, 1, dist0.size+1))
        rcParams['font.size'] = 12
        rcParams['xtick.direction'] = 'in'
        rcParams['ytick.direction'] = 'in'
        fig = plt.figure(figsize=(8, 6))
        ax3 = fig.add_subplot(111)

        ax3.plot(data_std[:, 0], data_std[:, 1]*0.01, label='Lamp', zorder=0)

        for indexCal in range(NFile):
            if indexCal == 0:
                ax3.plot(wvl_si, data2fit_si[indexCal, :, 0]/intTime_si[indexCal]/resp_si, color=colors[np.argmin(np.abs(dist[indexCal]-dist0))+1, :], zorder=1, label='%d cm' % dist[indexCal])
                ax3.plot(wvl_in, data2fit_in[indexCal, :, 0]/intTime_in[indexCal]/resp_in, color=colors[np.argmin(np.abs(dist[indexCal]-dist0))+1, :], zorder=1)
            elif dist[indexCal] == 50:
                ax3.plot(wvl_si, data2fit_si[indexCal, :, 0]/intTime_si[indexCal]/resp_si, color=colors[np.argmin(np.abs(dist[indexCal]-dist0))+1, :], zorder=1)
                ax3.plot(wvl_in, data2fit_in[indexCal, :, 0]/intTime_in[indexCal]/resp_in, color=colors[np.argmin(np.abs(dist[indexCal]-dist0))+1, :], zorder=1)
            else:
                ax3.plot(wvl_si, data2fit_si[indexCal, :, 0]/intTime_si[indexCal]/resp_si, color=colors[np.argmin(np.abs(dist[indexCal]-dist0))+1, :], zorder=1, label='%d cm' % dist[indexCal])
                ax3.plot(wvl_in, data2fit_in[indexCal, :, 0]/intTime_in[indexCal]/resp_in, color=colors[np.argmin(np.abs(dist[indexCal]-dist0))+1, :], zorder=1)

        ax3.axhline(0, color='k', ls=':')
        ax3.set_xlabel('Wavelength [nm]')
        ax3.set_ylabel('Irradiance [$\mathrm{W m^2 nm^{-1}}$]')
        ax3.set_xlim([350, 2200])
        ax3.set_ylim([0, 0.6])
        ax3.legend(loc='upper right', fontsize=11, framealpha=0.2)
        plt.savefig('fig.png')
    #plt.show()
    #exit()

    if plt_logic:
    #if True:
        rcParams['font.size'] = 12
        rcParams['xtick.direction'] = 'in'
        rcParams['ytick.direction'] = 'in'
        fig = plt.figure(figsize=(16, 5))
        gs  = gridspec.GridSpec(1, 3)
        ax1 = plt.subplot(gs[0, 0])
        ax2 = plt.subplot(gs[0, 1])
        ax3 = plt.subplot(gs[0, 2])
        ax1.plot(wvl_si, data2fit_si[indexCal, :, 0], color='k')
        ax1.axvline(wvl_ref, color='grey', ls='--')
        ax1.axhline(0, color='k', ls=':')
        ax1.set_title('Silicon')
        ax1.set_xlabel('Wavelength [nm]')
        ax1.set_ylabel('Dark Corrected Counts')
        ax1.set_xlim([200, 1200])
        ax1.set_ylim([0, 15000])
        #ax1.ticklabel_format(style='sci',axis='y', scilimits=(-3,4))

        ax2.plot(wvl_in, data2fit_in[indexCal, :, 0], color='k')
        ax2.axhline(0, color='k', ls=':')
        ax2.set_title('InGaAs')
        ax2.set_xlabel('Wavelength [nm]')
        ax2.set_ylabel('Dark Corrected Counts')
        ax2.set_xlim([500, 2500])
        #ax2.ticklabel_format(style='sci',axis='y', scilimits=(-3,4))
        ax2.set_ylim([0, 20000])

        ax3.plot(data_std[:, 0], data_std[:, 1]*0.01, label='Lamp', zorder=0)
        ax3.plot(wvl_si, data2fit_si[indexCal, :, 0]/intTime_si[indexCal]/resp_si, color='red', alpha=0.4, zorder=1)
        ax3.fill_between(wvl_si, (data2fit_si[indexCal, :, 0]-data2fit_si[indexCal, :, 1])/intTime_si[indexCal]/resp_si, (data2fit_si[indexCal, :, 0]+data2fit_si[indexCal, :, 1])/intTime_si[indexCal]/resp_si, facecolor='r', alpha=0.4, lw=0, label='Cal Si', zorder=1)

        ax3.plot(wvl_in, data2fit_in[indexCal, :, 0]/intTime_in[indexCal]/resp_in, color='blue', alpha=0.4, zorder=1)
        ax3.fill_between(wvl_in, (data2fit_in[indexCal, :, 0]-data2fit_in[indexCal, :, 1])/intTime_in[indexCal]/resp_in, (data2fit_in[indexCal, :, 0]+data2fit_in[indexCal, :, 1])/intTime_in[indexCal]/resp_in, facecolor='b', alpha=0.4, lw=0, label='Cal In', zorder=1)
        ax3.axhline(0, color='k', ls=':')
        ax3.set_title('Lamp Spectrum')
        ax3.set_xlabel('Wavelength [nm]')
        ax3.set_ylabel('Irradiance [$\mathrm{W m^2 nm^{-1}}$]')
        ax3.set_xlim([350, 2200])
        ax3.set_ylim([0, 0.3])
        ax3.legend(loc='upper right', fontsize=11, framealpha=0.2)
        plt.savefig('fig0.png')
    #plt.show()
    #exit()

    if plt_logic:
    #if True:
        for indexCal in range(NFile):
            rcParams['font.size'] = 12
            rcParams['xtick.direction'] = 'in'
            rcParams['ytick.direction'] = 'in'
            fig = plt.figure(figsize=(16, 5))
            gs  = gridspec.GridSpec(1, 3)
            ax1 = plt.subplot(gs[0, 0])
            ax2 = plt.subplot(gs[0, 1])
            ax3 = plt.subplot(gs[0, 2])

            ax1.plot(wvl_si, data2fit_si[indexCal, :, 0]/intTime_si[indexCal]/resp_si, color='red', alpha=0.6, zorder=1)
            ax1.fill_between(wvl_si, (data2fit_si[indexCal, :, 0]-data2fit_si[indexCal, :, 1])/intTime_si[indexCal]/resp_si, (data2fit_si[indexCal, :, 0]+data2fit_si[indexCal, :, 1])/intTime_si[indexCal]/resp_si, facecolor='r', alpha=0.4, lw=0, label='Cal Si', zorder=1)
            ax1.plot(wvl_in, data2fit_in[indexCal, :, 0]/intTime_in[indexCal]/resp_in, color='blue', alpha=0.6, zorder=1)
            ax1.fill_between(wvl_in, (data2fit_in[indexCal, :, 0]-data2fit_in[indexCal, :, 1])/intTime_in[indexCal]/resp_in, (data2fit_in[indexCal, :, 0]+data2fit_in[indexCal, :, 1])/intTime_in[indexCal]/resp_in, facecolor='b', alpha=0.4, lw=0, label='Cal In', zorder=1)
            ax1.axvline(wvl_ref, color='grey', ls='--')
            ax1.set_title('Spectrum (Distance=%.1fcm)' % dist[indexCal])
            ax1.set_xlabel('Wavelength [nm]')
            ax1.set_ylabel('Irradiance [$\mathrm{W m^2 nm^{-1}}$]')
            ax1.set_xlim([350, 2200])
            ax1.set_ylim([0, 0.5])

            ax2.plot(wvl_in, data2fit_in[indexCal, :, 0], color='b', alpha=0.6)
            ax2.set_title('InGaAs')
            ax2.set_xlabel('Wavelength [nm]')
            ax2.set_ylabel('Dark Corrected Counts')
            ax2.set_xlim([500, 2500])
            ax2.set_ylim([0, 35000])
            #ax2.ticklabel_format(style='sci',axis='y', scilimits=(-3,4))

            ax3.plot(wvl_si, data2fit_si[indexCal, :, 0]/intTime_si[indexCal]/resp_si/resp_si_interp, color='r', alpha=0.6, zorder=1)
            ax3.fill_between(wvl_si, (data2fit_si[indexCal, :, 0]-data2fit_si[indexCal, :, 1])/intTime_si[indexCal]/resp_si/resp_si_interp, (data2fit_si[indexCal, :, 0]+data2fit_si[indexCal, :, 1])/intTime_si[indexCal]/resp_si/resp_si_interp, facecolor='r', alpha=0.4, lw=0, label='Si', zorder=1)
            ax3.plot(wvl_in, data2fit_in[indexCal, :, 0]/intTime_in[indexCal]/resp_in/resp_in_interp, color='b', alpha=0.6, zorder=2)
            ax3.fill_between(wvl_in, (data2fit_in[indexCal, :, 0]-data2fit_in[indexCal, :, 1])/intTime_in[indexCal]/resp_in/resp_in_interp, (data2fit_in[indexCal, :, 0]+data2fit_in[indexCal, :, 1])/intTime_in[indexCal]/resp_in/resp_in_interp, facecolor='b', alpha=0.4, lw=1, label='In', zorder=1)
            #print((data2fit_in[indexCal, :, 0]-data2fit_in[indexCal, :, 1])/intTime_in[indexCal]/resp_in/resp_in_interp)
            #print((data2fit_in[indexCal, :, 0]+data2fit_in[indexCal, :, 1])/intTime_in[indexCal]/resp_in/resp_in_interp)
            ax3.axvline(wvl_ref, color='grey', ls=':')
            ax3.axhline(1.0, color='k', zorder=0)
            ax3.axhline((50.0/dist[indexCal])**2, color='g', ls='--', alpha=0.6)
            ax3.axhline((50.0/dist[indexCal])**2 * 1.03, color='g', ls='--', alpha=0.6, zorder=0)
            ax3.axhline((50.0/dist[indexCal])**2 * 0.97, color='g', ls='--', alpha=0.6, zorder=0)
            #ax3.set_title('Lamp Spectrum')
            ax3.set_xlabel('Wavelength [nm]')
            ax3.set_ylabel('Ratio [Measurement/Lamp]')
            ax3.set_xlim([350, 1600])
            ylim_mid = np.round((50.0/dist[indexCal])**2, decimals=1)
            ax3.set_ylim([ylim_mid-0.4, ylim_mid+0.4])
            plt.savefig('fig1_dist%2.2d.png' % indexCal)
            plt.close(fig)
            #}}}

    poly_degree = 3
    coef_linear = np.zeros((NChan, 2), dtype=np.float64)
    coef_poly   = np.zeros((NChan, poly_degree+1), dtype=np.float64)
    nlin_deg    = np.zeros((NChan, 2), dtype=np.float64)
    x_minmax    = np.zeros((NChan, 2), dtype=np.float64)
    index_wvl_ref = np.argmin(np.abs(wvl_si-wvl_ref))
    for iChan in range(NChan):
        interp_x0 = data2fit_in[:, iChan, 0] / std_in[iChan]
        interp_y0 = data2fit_si[:, index_wvl_ref, 0] / std_si[index_wvl_ref]
        index_sort = np.argsort(interp_x0)
        interp_x = interp_x0[index_sort]
        interp_y = interp_y0[index_sort]

        coef_linear[iChan, 1], coef_linear[iChan, 0], r_value, p_value, std_err = stats.linregress(interp_x, interp_y)
        coef_poly[iChan, ::-1] = np.polyfit(interp_x, interp_y, poly_degree)

        x_minmax[iChan, 0] = np.min(interp_x)
        x_minmax[iChan, 1] = np.max(interp_x)

        lin_y  = coef_linear[iChan, 0] + coef_linear[iChan, 1]*interp_x
        nlin_y = np.zeros_like(lin_y)
        for exp in range(poly_degree+1):
            nlin_y += coef_poly[iChan, exp]*interp_x**exp

        nlin_deg[iChan, 0] = np.max(np.abs(( lin_y/(data2fit_si[:, index_wvl_ref, 0]/std_si[index_wvl_ref])-1.0)*100.0))
        nlin_deg[iChan, 1] = np.max(np.abs((nlin_y/(data2fit_si[:, index_wvl_ref, 0]/std_si[index_wvl_ref])-1.0)*100.0))

    if plt_logic:
    #if True:
        for indexCal in range(NChan):
            rcParams['font.size'] = 12
            rcParams['xtick.direction'] = 'in'
            rcParams['ytick.direction'] = 'in'
            fig = plt.figure(figsize=(11, 5))
            gs  = gridspec.GridSpec(1, 2)
            ax1 = plt.subplot(gs[0, 0])
            ax2 = plt.subplot(gs[0, 1])

            ax1.scatter(data2fit_si[:, index_wvl_ref, 0]/std_si[index_wvl_ref], data2fit_in[:, indexCal, 0]/std_in[indexCal], edgecolor='k', facecolor='none', alpha=0.6, s=60, zorder=1, marker='s')

            xerr = (data2fit_si[:, index_wvl_ref, 1])/std_si[index_wvl_ref]
            yerr = (data2fit_in[:, indexCal     , 1])/std_in[indexCal     ]
            ax1.errorbar(data2fit_si[:, index_wvl_ref, 0]/std_si[index_wvl_ref], data2fit_in[:, indexCal, 0]/std_in[indexCal], xerr=xerr, yerr=yerr, fmt='none')

            ax1.plot([1.0, 1.0], [0.8, 1.2], color='g')
            ax1.plot([0.8, 1.2], [1.0, 1.0], color='g')
            ax1.plot([-10, 10], [-10, 10], color='k', ls=':')

            xx = np.linspace(-10, 10, 1000)
            lin_y    = coef_linear[indexCal, 0] + coef_linear[indexCal, 1]*xx
            nlin_y = np.zeros_like(lin_y)
            for exp in range(poly_degree+1):
                nlin_y += coef_poly[indexCal, exp]*xx**exp
            ax1.plot( lin_y, xx, color='r', alpha=0.6, label='lin  fit')
            ax1.plot(nlin_y, xx, color='b', alpha=0.6, label='poly fit')
            ax1.legend(loc='upper left', fontsize=8, framealpha=0.3)

            ax1.set_title('Channel %d' % (indexCal+1))
            ax1.set_xlabel('Silicon/Silicon_Cal_50cm')
            ax1.set_ylabel('InGaAs/InGaAs_Cal_50cm')
            ax1.set_xlim([0.0, 2.5])
            ax1.set_ylim([0.0, 2.5])

            ax2.axvline(1.0, color='k', ls='--')
            ax2.axhline( 0.3, color='g', ls=':')
            ax2.axhline(-0.3, color='g', ls=':')
            xx = data2fit_in[:, indexCal, 0] / std_in[indexCal]
            lin_y    = coef_linear[indexCal, 0] + coef_linear[indexCal, 1]*xx
            nlin_y = np.zeros_like(lin_y)
            for exp in range(poly_degree+1):
                nlin_y += coef_poly[indexCal, exp]*xx**exp

            ax2.scatter(xx, (    xx/(data2fit_si[:, index_wvl_ref, 0]/std_si[index_wvl_ref])-1.0)*100.0, c='k', s=50, alpha=0.4, label='residual from 1:1')
            ax2.scatter(xx, ( lin_y/(data2fit_si[:, index_wvl_ref, 0]/std_si[index_wvl_ref])-1.0)*100.0, c='r', s=50, alpha=0.9, label='residual from lin')
            ax2.scatter(xx, (nlin_y/(data2fit_si[:, index_wvl_ref, 0]/std_si[index_wvl_ref])-1.0)*100.0, c='b', s=50, alpha=0.9, label='residual from nlin')

            yy = data2fit_si[:, index_wvl_ref, 0] / std_si[index_wvl_ref]
            ax2.scatter(xx, yy, edgecolor='k', facecolor='none', alpha=0.6, s=60, zorder=1, marker='s', label='Si Ref')

            xerr = (data2fit_in[:, indexCal, 1]/std_in[indexCal])
            xx = (data2fit_in[:, indexCal, 0]/std_in[indexCal])
            yy = (nlin_y/(data2fit_si[:,index_wvl_ref,0]/std_si[index_wvl_ref])-1.0)*100.0
            ax2.errorbar(xx, yy, xerr=xerr, yerr=0, fmt='none')

            ax2.set_xlim([0.0, 2.5])
            ax2.legend(loc='lower right', fontsize=8, framealpha=0.3)
            ax2.set_xlabel('InGaAs/InGaAs_Cal_50cm')
            ax2.set_ylabel('Non-linearity Degree [%]')
            ax2.set_title('Wavelength %.2f nm' % wvl_in[indexCal])
            plt.savefig('fig2_chan%3.3d.png' % (indexCal+1))
            plt.close(fig)
            print(indexCal)
            #plt.show()

    plt_logic = False
    if plt_logic:
        rcParams['font.size'] = 12
        rcParams['xtick.direction'] = 'in'
        rcParams['ytick.direction'] = 'in'
        fig = plt.figure(figsize=(11, 5))
        ax1 = fig.add_subplot(211)
        ax2 = fig.add_subplot(212)

        ax1.plot(wvl_in, coef_poly[:, -1])
        ax1.axhline(0.0, color='red', lw=0.8, ls=':')
        ax1.set_ylabel('Quadratic Term')
        ax1.set_ylim([-0.1, 0.1])

        #ax2.plot(wvl_in, nlin_deg[:, 1])
        #ax2.set_ylim([162, 164])
        #offset = np.zeros((NFile, NChan), dtype=np.float64)
        #for i in range(NFile):
            #xx = dist[i]
            #for exp in range(poly_degree+1):
                #nlin_y += coef_poly[:, exp]*xx**exp

        #offset = np.array([])
        #for indexCal in range(NChan):
            #xx = data2fit_in[:, indexCal, 0] / std_in[indexCal]
            #nlin_y = np.zeros_like(lin_y)
            #for exp in range(poly_degree+1):
                #nlin_y += coef_poly[indexCal, exp]*xx**exp
            #offset = np.append(offset, nliny-)


        ax2.set_ylabel('Offset')
        ax2.set_xlabel('Wavelength [nm]')
        plt.savefig('quadratic.png')
        #plt.show()

    return wvl_in, coef_poly

if __name__ == '__main__':

    import matplotlib as mpl
    #mpl.use('Agg')
    from matplotlib.ticker import FixedLocator, MaxNLocator
    import matplotlib.gridspec as gridspec
    import matplotlib.pyplot as plt
    from matplotlib import rcParams
    # plt.plot(np.arange(iaa.size), dc)
    # plt.show()
    # exit()

    fnames = sorted(glob.glob('data/ssfr/*.SKS'))

    sks    = READ_SKS_TEST(fnames, dateRef=datetime.datetime(2014, 10, 2), verbose=True)
    # logic_cir1 = (sks.tmhr_corr>=24.15)&(sks.tmhr_corr<=24.65)
    # logic_cir2 = (sks.tmhr_corr>=25.00)&(sks.tmhr_corr<=25.56)

    # fig = plt.figure(figsize=(6, 6))

    # logic = sks.shutter==0
    # plt.scatter(sks.tmhr_corr[logic], sks.)
    exit()

    # sks    = READ_SKS(fnames, dateRef=datetime.datetime(2014, 10, 2), verbose=True)
    # logic_cir1 = (sks.tmhr_corr>=24.15)&(sks.tmhr_corr<=24.65)
    # f = h5py.File('test_cir1_raw.h5', 'w')
    # f['spectra'] = sks.spectra[logic_cir1, ...]
    # f['shutter'] = sks.shutter[logic_cir1, ...]
    # f['int_time'] = sks.int_time[logic_cir1, ...]
    # f['temp']     = sks.temp[logic_cir1, ...]
    # f['jday_NSF'] = sks.jday_NSF[logic_cir1, ...]
    # f['jday_cRIO'] = sks.jday_cRIO[logic_cir1, ...]
    # f['qual_flag'] = sks.qual_flag[logic_cir1, ...]
    # f['jday_corr'] = sks.jday_corr[logic_cir1, ...]
    # f['tmhr_corr'] = sks.tmhr_corr[logic_cir1, ...]
    # f['spectra_dark_corr'] = sks.spectra_dark_corr[logic_cir1, ...]
    # f.close()

    # logic_cir2 = (sks.tmhr_corr>=25.00)&(sks.tmhr_corr<=25.60)
    # f = h5py.File('test_cir2_raw.h5', 'w')
    # f['spectra'] = sks.spectra[logic_cir2, ...]
    # f['shutter'] = sks.shutter[logic_cir2, ...]
    # f['int_time'] = sks.int_time[logic_cir2, ...]
    # f['temp']     = sks.temp[logic_cir2, ...]
    # f['jday_NSF'] = sks.jday_NSF[logic_cir2, ...]
    # f['jday_cRIO'] = sks.jday_cRIO[logic_cir2, ...]
    # f['qual_flag'] = sks.qual_flag[logic_cir2, ...]
    # f['jday_corr'] = sks.jday_corr[logic_cir2, ...]
    # f['tmhr_corr'] = sks.tmhr_corr[logic_cir2, ...]
    # f['spectra_dark_corr'] = sks.spectra_dark_corr[logic_cir2, ...]
    # f.close()
