import os
import glob
import datetime
import numpy as np
import h5py
from termcolor import colored, cprint
import multiprocessing as mp
from collections import OrderedDict

class libRadtran_INIT:

    """
    Python wrapper for libRadtran

    Tested on macOS v10.12.3 with
    Python v3.6.0 :: Anaconda 4.3.1 (x86_64)
    libRadtran version: 2.0.1
    """

    def __init__(self, \
            tag,
            cld_cfg   = None,
            aer_cfg   = None,
            fdir_bin  = '/data/hong/soft/libradtran/v2.0.1/bin',
            fdir_data = '/data/hong/soft/libradtran/v2.0.1/data',
            fdir_out  = 'data/tmp/libradtran',
            mode      = 'read',
            verbose   = False
            ):

        bad_txt  = colored('Error   [libRadtra_INIT.__init__]', 'red')
        good_txt = colored('Message [libRadtra_INIT.__init__]', 'green')
        okay_txt = colored('Warning [libRadtra_INIT.__init__]', 'magenta')

        self.cfg = OrderedDict([
                ('atmosphere_file'   , None),
                ('source solar'      , None),
                ('day_of_year'       , None),
                ('albedo'            , None),
                ('sza'               , None),
                ('rte_solver'        , None),
                ('number_of_streams' , None),
                ('wavelength'        , None),
                ('slit_function_file', None),
                ('zout'              , None),
                ])

        if cld_cfg is None:
            tag = '%s_%s' % (tag, 'Ncld')
        else:
            tag = '%s_%s' % (tag, 'Ycld')

        if aer_cfg is None:
            tag = '%s_%s' % (tag, 'Naer')
        else:
            tag = '%s_%s' % (tag, 'Yaer')

        self.tag       = tag
        self.fdir_bin  = fdir_bin
        self.fdir_data = fdir_data

        self.fdir_out  = fdir_out
        if not os.path.exists(self.fdir_out):
            os.system('mkdir -p %s' % self.fdir_out)

        fname_exe   = '%s/uvspec' % fdir_bin
        if os.path.isfile(fname_exe):
            self.fname_exe = fname_exe
        else:
            exit(bad_txt+': cannot find executable file for libRadtran.')

        fname_atm   = '%s/atmmod/afglss.dat' % fdir_data
        if os.path.isfile(fname_atm):
            self.cfg['atmosphere_file'] = fname_atm
            if verbose:
                print(good_txt+': atmosphere_file set to %s.' % fname_atm)
        else:
            exit(bad_txt+': cannot find atm file for libRadtran.')

        # fname_solar = '%s/solar_flux/kurudz_0.1nm.dat' % self.fdir_data
        fname_solar = '/data/hong/data/other/kurudz_0.1nm_hchen.dat'
        # fname_solar = '%s/solar_flux/kurudz_1.0nm.dat' % self.fdir_data
        # fname_solar = '/Users/hoch4240/Chen/other/data/aux_libradtran/kurudz_full.dat'
        if os.path.isfile(fname_solar):
            self.cfg['source solar'] = fname_solar
            if verbose:
                print(good_txt+': solar file set to %s.' % fname_solar)
        else:
            exit(bad_txt+': cannot find solar file for libRadtran.')

        self.fname_in  = '%s/%s_inp.txt' % (self.fdir_out, self.tag)
        self.fname_out = '%s/%s_out.txt' % (self.fdir_out, self.tag)
        if mode == 'write':
            os.system('rm -rf %s' % self.fname_out)

def libRadtran_RUN(init, verbose=False):

    bad_txt  = colored('Error   [libRadtra_RUN]', 'red')
    good_txt = colored('Message [libRadtra_RUN]', 'green')
    okay_txt = colored('Warning [libRadtra_RUN]', 'magenta')

    # write input file for libRadtran
    f = open(init.fname_in, 'w')
    for key in init.cfg.keys():
        if init.cfg[key] is None:
            exit(bad_txt+': %s is not set.' % key)
        else:
            f.write('%-20s %s\n' % (key, init.cfg[key]))

    # if wvl >= 850.0:
        # f.write('correlated_k         LOWTRAN\n')
    # if wvl < 940.0:
        # f.write('slit_function_file   %s\n' % init.fname_vis)
    # else:
        # f.write('slit_function_file   %s\n' % init.fname_nir)
    if verbose:
        f.write('verbose')
    else:
        f.write('quiet')
    f.close()

    # make run
    os.system('%s < %s > %s' % (init.fname_exe, init.fname_in, init.fname_out))

def libRadtran_MP(inits, ncpu=15):

    pool = mp.Pool(processes=ncpu)
    pool.outputs = pool.map(libRadtran_RUN, inits)
    pool.close()
    pool.join()

class libRadtran_GDATA:

    def __init__(self, inits):

        self.f_down_direct  = np.array([])
        self.f_down_diffuse = np.array([])
        self.f_down         = np.array([])
        self.f_up           = np.array([])

        for init in inits:

            data = np.loadtxt(init.fname_out).reshape((-1, 7))
            f_down_direct0  = data[:, 1]
            f_down_diffuse0 = data[:, 2]
            f_down0         = f_down_direct0 + f_down_diffuse0
            f_up0           = data[:, 3]

            self.f_down_direct  = np.append(self.f_down_direct , f_down_direct0)
            self.f_down_diffuse = np.append(self.f_down_diffuse, f_down_diffuse0)
            self.f_down         = np.append(self.f_down        , f_down0)
            self.f_up           = np.append(self.f_up          , f_up0)

def libRadtran_CDATA_H5(tau, cldref, wvls, albedos, szas, tag, fdir='data'):

    # albedo modification
    if tag == 'sea':
        logic = (wvls>806.0)
        albedos[logic] = 0.037
    if tag=='ice':
        logic = (wvls>1464.0)
        albedos[logic] = 0.0

    wvls = np.round(wvls, decimals=0)

    init   = libRadtran_INIT(tau, cldref=cldref)
    libRadtran_MP(init, wvls, albedos, szas)
    data   = libRadtran_GDATA(init, wvls)

    fname = '%s/libRadtran_%04.1f_%04.1f_%s.h5' % (fdir, tau, cldref, tag)
    f     = h5py.File(fname, 'w')
    f['wvl']            = data.wvls
    f['f_down_direct']  = data.f_down_direct
    f['f_down_diffuse'] = data.f_down_diffuse
    f['f_up']           = data.f_up
    f.close()

    print('%s is created.' % fname)

class libRadtran_READ_H5:

    def __init__(self, tau, cldref, tag, fdir='data', topN=-2, bottomN=1):

        self.fname = '%s/libRadtran_%04.1f_%04.1f_%s.h5' % (fdir, tau, cldref, tag)

        f = h5py.File(self.fname, 'r')
        self.f_down_direct  = f['f_down_direct'][...].reshape((-1, 7))*0.001
        self.f_down_diffuse = f['f_down_diffuse'][...].reshape((-1, 7))*0.001
        self.f_up           = f['f_up'][...].reshape((-1, 7))*0.001
        self.wvl            = f['wvl'][...]
        f.close()

        self.f_down         = self.f_down_direct + self.f_down_diffuse
        self.transmittance  = self.f_down[:, bottomN]/self.f_down[:, topN] * 100.0
        self.albedo_bottom  = self.f_up[:, bottomN]/self.f_down[:, bottomN] * 100.0
        self.albedo_top     = self.f_up[:, topN]/self.f_down[:, topN] * 100.0
        f_net_top           = self.f_down[:,topN] - self.f_up[:,topN]
        f_net_bottom        = self.f_down[:, bottomN] - self.f_up[:, bottomN]
        self.absorptance    = (f_net_top-f_net_bottom)/self.f_down[:, topN] * 100.0
        self.reflectance    = (self.f_up[:, topN] - self.f_up[:, bottomN]) / self.f_down[:, topN] * 100.0
        self.f_bottom_down  = self.f_down[:, bottomN].copy()
        self.f_top_down     = self.f_down[:, topN].copy()
        self.f_bottom_up    = self.f_up[:, bottomN].copy()
        self.f_top_up       = self.f_up[:, topN].copy()

def CDATA_SSFR():
    fname_nir = '/Users/hoch4240/Chen/other/data/aux_ssfr/nir_1nm_s.dat'
    fname_vis = '/Users/hoch4240/Chen/other/data/aux_ssfr/vis_1nm_s.dat'

    if os.path.isfile(fname_vis):
        fname_vis = fname_vis
    else:
        fname_vis = None

    if os.path.isfile(fname_nir):
        fname_nir = fname_nir
    else:
        fname_nir = None

    dtime = datetime.datetime(2014, 10, 3)
    doy   = (dtime-datetime.datetime(dtime.year-1, 12, 31)).days

    from pre_ssfr import READ_SKS_TEST
    fnames = sorted(glob.glob('data/ssfr/*.SKS'))
    sks    = READ_SKS_TEST(fnames, dateRef=datetime.datetime(2014, 10, 2), verbose=True)

    # inits = []
    # for i in range(sks.sza0.size):
    #     sza0 = sks.sza0[i]
    #     saa0 = sks.saa0[i]
    #     alt0 = sks.alt0[i]/1000.0
    #     init = libRadtran_INIT('%7.7d' % i)
    #     init.cfg['day_of_year'] = str(doy)
    #     init.cfg['wavelength'] = '666.0 666.0'
    #     init.cfg['albedo']     = str(0.03)
    #     init.cfg['sza']        = str(sza0)
    #     init.cfg['rte_solver'] = 'disort'
    #     init.cfg['number_of_streams']  = str(6)
    #     init.cfg['slit_function_file'] = fname_vis
    #     init.cfg['zout'] = str(alt0)
    #     inits.append(init)

    flux_lrt = np.zeros_like(sks.spectra_flux_zen)
    for i in range(flux_lrt.shape[0]):
        sza0 = sks.sza0[i]
        saa0 = sks.saa0[i]
        alt0 = sks.alt0[i]/1000.0

        inits = []
        for j in range(sks.wvl_zen.size):
            init = libRadtran_INIT('%7.7d' % j, mode='write')
            init.cfg['day_of_year'] = str(doy)
            wvl_round = str(np.round(sks.wvl_zen[j], decimals=1))
            init.cfg['wavelength'] = '%s %s' % (wvl_round, wvl_round)
            init.cfg['albedo']     = str(0.03)
            init.cfg['sza']        = str(sza0)
            init.cfg['rte_solver'] = 'disort'
            init.cfg['number_of_streams']  = str(6)
            if sks.wvl_zen[i] < 940.0:
                init.cfg['slit_function_file'] = fname_vis
            else:
                init.cfg['slit_function_file'] = fname_nir
            init.cfg['zout'] = str(alt0)
            inits.append(init)
        libRadtran_MP(inits)
        lrt    = libRadtran_GDATA(inits)
        flux_lrt[i, :] = lrt.f_down/1000.0
        print(i/flux_lrt.shape[0])

    f = h5py.File('lrt_flux.h5', 'w')
    f['flux_lrt'] = flux_lrt
    f.close()

def CDATA_CIR1():

    # fname_nir = '/data/hong/work/02_arise/cal/data/aux_ssfr/nir_0.1nm_s.dat'
    # fname_vis = '/data/hong/work/02_arise/cal/data/aux_ssfr/vis_0.1nm_s.dat'
    fname_nir = '/data/hong/work/02_arise/cal/data/aux_ssfr/nir_1nm_s.dat'
    fname_vis = '/data/hong/work/02_arise/cal/data/aux_ssfr/vis_1nm_s.dat'

    if os.path.isfile(fname_vis):
        fname_vis = fname_vis
    else:
        fname_vis = None

    if os.path.isfile(fname_nir):
        fname_nir = fname_nir
    else:
        fname_nir = None

    dtime = datetime.datetime(2014, 10, 3)
    doy   = (dtime-datetime.datetime(dtime.year-1, 12, 31)).days

    from pre_ssfr import READ_SKS_TEST
    fnames = sorted(glob.glob('data/ssfr/*.SKS'))
    sks    = READ_SKS_TEST(fnames, dateRef=datetime.datetime(2014, 10, 2), verbose=True)

    f = h5py.File('test_cir2_lrt.h5', 'r')
    wvl_lrt = f['wvl_lrt'][...]
    f.close()
    # inits = []
    # for i in range(sks.sza0.size):
    #     sza0 = sks.sza0[i]
    #     saa0 = sks.saa0[i]
    #     alt0 = sks.alt0[i]/1000.0
    #     init = libRadtran_INIT('%7.7d' % i)
    #     init.cfg['day_of_year'] = str(doy)
    #     init.cfg['wavelength'] = '666.0 666.0'
    #     init.cfg['albedo']     = str(0.03)
    #     init.cfg['sza']        = str(sza0)
    #     init.cfg['rte_solver'] = 'disort'
    #     init.cfg['number_of_streams']  = str(6)
    #     init.cfg['slit_function_file'] = fname_vis
    #     init.cfg['zout'] = str(alt0)
    #     inits.append(init)

    flux_lrt = np.zeros((sks.spectra_flux_zen.shape[0], wvl_lrt.size), dtype=np.float64)

    for i in range(flux_lrt.shape[0]):
        sza0 = sks.sza0[i]
        saa0 = sks.saa0[i]
        alt0 = sks.alt0[i]/1000.0

        inits = []
        for j in range(wvl_lrt.size):
            init = libRadtran_INIT('%7.7d' % j, mode='write')
            init.cfg['day_of_year'] = str(doy)
            wvl_round = str(np.round(wvl_lrt[j], decimals=1))
            init.cfg['wavelength'] = '%s %s' % (wvl_round, wvl_round)
            init.cfg['albedo']     = str(0.03)
            init.cfg['sza']        = str(sza0)
            init.cfg['rte_solver'] = 'disort'
            init.cfg['number_of_streams']  = str(6)
            if wvl_lrt[j] < 940.0:
                init.cfg['slit_function_file'] = fname_vis
            else:
                init.cfg['slit_function_file'] = fname_nir
            init.cfg['zout'] = str(alt0)
            inits.append(init)
        libRadtran_MP(inits)
        lrt    = libRadtran_GDATA(inits)
        flux_lrt[i, :] = lrt.f_down/1000.0
        print(i/flux_lrt.shape[0] * 100.0, "%")

    f = h5py.File('test_cir1_lrt.h5', 'w')
    f['flux_lrt'] = flux_lrt
    f['wvl_lrt']  = wvl_lrt
    f.close()

if __name__ == '__main__':
    CDATA_CIR1()
    exit()





    import matplotlib as mpl
    import matplotlib.pyplot as plt
    from matplotlib.ticker import FixedLocator
    from mpl_toolkits.basemap import Basemap

    f = h5py.File('flux_lrt.h5', 'r')
    flux_lrt = f['flux_lrt'][...]
    wvl_lrt  = f['wvl_lrt'][...]
    f.close()

    wvlRange = [200, 4000]
    indices  = (wvl_lrt>=wvlRange[0])&(wvl_lrt<=wvlRange[1])
    wvl      = wvl_lrt[indices]

    bbr_lrt  = np.zeros(flux_lrt.shape[0], dtype=np.float64)
    for i in range(flux_lrt.shape[0]):
        flux       = flux_lrt[i, :][indices]
        bbr_lrt[i] = np.trapz(flux, x=wvl)

    plt.scatter(np.arange(flux_lrt.shape[0]), bbr)
    plt.show()
    exit()

    # fig = plt.figure(figsize=(10, 6))
    # ax1 = fig.add_subplot(111)
    # ax1.scatter(sks.wvl_zen, sks.spectra_flux_zen[1000, :], label='SSFR no CC', alpha=0.4)
    # ax1.scatter(sks.wvl_zen, sks.spectra_flux_zen[1000, :]*sks.cos_corr_factor0, label='SSFR CC (modified)', alpha=0.4)
    # ax1.scatter(sks.wvl_zen, sks.spectra_flux_zen[1000, :]*sks.cos_corr_factor_ori0, label='SSFR CC(original)', alpha=0.4)
    # ax1.scatter(sks.wvl_zen, lrt.f_down/1000.0, label='libRadtran', alpha=0.4)
    # # ax1.scatter(sks.tmhr_corr, sks.flux_corr, label='SSFR CC (modified)', alpha=0.4)
    # # ax1.scatter(sks.tmhr_corr, sks.flux_corr_ori, label='SSFR CC (original)', alpha=0.4)
    # # ax1.scatter(sks.tmhr_corr, lrt.f_down/1000.0, label='libRadtran')
    # # ax1.set_xlim((25.0, 25.6))
    # # ax1.set_ylim((0.2, 0.8))
    # ax1.set_xlabel('Wavelength [nm]')
    # ax1.set_ylabel('Flux [$\mathrm{W m^{-2} nm^{-1}}$]')
    # plt.legend()
    # plt.savefig('cir2-spec.png')
    # plt.show()
    # exit()

    # figure settings
    fig = plt.figure(figsize=(10, 6))
    ax1 = fig.add_subplot(111)
    ax1.scatter(sks.tmhr_corr, sks.flux_ori, label='SSFR no CC', alpha=0.4)
    ax1.scatter(sks.tmhr_corr, sks.flux_corr, label='SSFR CC (modified)', alpha=0.4)
    ax1.scatter(sks.tmhr_corr, sks.flux_corr_ori, label='SSFR CC (original)', alpha=0.4)
    ax1.scatter(sks.tmhr_corr, lrt.f_down/1000.0, label='libRadtran')
    ax1.set_xlim((25.0, 25.6))
    ax1.set_ylim((0.2, 0.8))
    ax1.set_xlabel('Time [hour]')
    ax1.set_ylabel('Flux [$\mathrm{W m^{-2} nm^{-1}}$]')
    plt.legend()
    plt.savefig('cir2-lrt.png')
    plt.show()
