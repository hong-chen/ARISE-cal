import os
import sys
import glob
import h5py
import numpy as np
import datetime
from scipy.io import readsav
from scipy import interpolate
import pysolar

class READ_BBR:
    def __init__(self, fname, skip_header=56, delimiter=','):

        bband = np.genfromtxt(fname, skip_header=skip_header, delimiter=delimiter)
        self.tmhr = bband[:, 0]/3600.0
        self.lwd  = bband[:, 1]
        self.lwu  = bband[:, 2]
        self.swd  = bband[:, 3]
        self.swu  = bband[:, 4]
        self.spn  = bband[:, 5]
        self.dif  = bband[:, 6]

class READ_BBR_V1:
    def __init__(self, fname, skip_header=1, delimiter=None):
        """
        SPN1: Sunshine Pyranometer
        Global and Diffuse
        """

        bband = np.genfromtxt(fname, skip_header=skip_header, delimiter=delimiter)
        tmhr = (bband[:, 0]/3600.0)
        # logic = (tmhr>=25.00) & (tmhr<=25.60)
        logic = (tmhr>=24.15) & (tmhr<=24.65)
        self.tmhr                              = (tmhr)[logic]
        self.zen_sw_flux                       = (bband[:, 1])[logic]
        self.zen_sw_flux_noturn                = (bband[:, 2])[logic]
        self.zen_sw_flux_noturn_attcorr        = (bband[:, 3])[logic]
        self.nad_sw_flux                       = (bband[:, 4])[logic]
        self.nad_sw_flux_noturn                = (bband[:, 5])[logic]
        self.zen_spn_total_flux                = (bband[:, 6])[logic]
        self.zen_spn_total_flux_noturn         = (bband[:, 7])[logic]
        self.zen_spn_total_flux_noturn_attcorr = (bband[:, 8])[logic]
        self.zen_spn_diff_flux                 = (bband[:, 9])[logic]
        self.zen_spn_diff_flux_noturn          = (bband[:, 10])[logic]

def PLT_ZEN_SW():
    fname = 'data/bbr/20141002_C130_attcorr.txt'
    bbr = READ_BBR_V1(fname)

    fig = plt.figure(figsize=(8, 6))
    ax1 = fig.add_subplot(111)
    ax1.scatter(bbr.tmhr, bbr.zen_sw_flux               , s=50, label='turn'           , alpha=0.3, facecolor='none', edgecolor='r', linewidth=0.8)
    ax1.scatter(bbr.tmhr, bbr.zen_sw_flux_noturn        , s=50, label='no turn'        , alpha=0.3, facecolor='none', edgecolor='b', linewidth=0.8)
    ax1.scatter(bbr.tmhr, bbr.zen_sw_flux_noturn_attcorr, s=50, label='no turn attcorr', alpha=0.3, facecolor='none', edgecolor='g', linewidth=0.8)
    ax1.set_xlim([25.0, 25.6])
    ax1.set_ylim([200,  600])
    ax1.set_xlabel('Time [hour]')
    ax1.set_ylabel('Flux [$\mathrm{W m^{-2}}$]')
    ax1.set_title('Zenith Shortwave Flux')
    ax1.legend(loc='best', fontsize=14, framealpha=0.4, markerscale=1)
    plt.savefig('bbr_zen_sw_flux.png')
    # plt.show()

def PLT_NAD_SW():
    fname = 'data/bbr/20141002_C130_attcorr.txt'
    bbr = READ_BBR_V1(fname)

    fig = plt.figure(figsize=(8, 6))
    ax1 = fig.add_subplot(111)
    ax1.scatter(bbr.tmhr, bbr.nad_sw_flux        , s=50, label='turn'        , alpha=0.3, facecolor='none', edgecolor='r', linewidth=0.8)
    ax1.scatter(bbr.tmhr, bbr.nad_sw_flux_noturn , s=50, label='no turn'     , alpha=0.3, facecolor='none', edgecolor='g', linewidth=0.8)
    ax1.set_xlim([25.0, 25.6])
    ax1.set_ylim([20,  100])
    ax1.set_xlabel('Time [hour]')
    ax1.set_ylabel('Flux [$\mathrm{W m^{-2}}$]')
    ax1.set_title('Nadir Shortwave Flux')
    ax1.legend(loc='best', fontsize=14, framealpha=0.4, markerscale=1)
    plt.savefig('bbr_nad_sw_flux.png', transparent=False)
    # plt.show()

def PLT_ZEN_SPN_TOTAL():
    fname = 'data/bbr/20141002_C130_attcorr.txt'
    bbr = READ_BBR_V1(fname)

    fig = plt.figure(figsize=(8, 6))
    ax1 = fig.add_subplot(111)
    ax1.scatter(bbr.tmhr, bbr.zen_spn_total_flux               , s=50, label='turn'           , alpha=0.3, facecolor='none', edgecolor='r', linewidth=0.8)
    ax1.scatter(bbr.tmhr, bbr.zen_spn_total_flux_noturn        , s=50, label='no turn'        , alpha=0.3, facecolor='none', edgecolor='b', linewidth=0.8)
    ax1.scatter(bbr.tmhr, bbr.zen_spn_total_flux_noturn_attcorr, s=50, label='no turn attcorr', alpha=0.3, facecolor='none', edgecolor='g', linewidth=0.8)
    ax1.set_xlim([25.0, 25.6])
    ax1.set_ylim([200,  600])
    ax1.set_xlabel('Time [hour]')
    ax1.set_ylabel('Flux [$\mathrm{W m^{-2}}$]')
    ax1.set_title('Zenith SPN Total Flux')
    ax1.legend(loc='best', fontsize=12, framealpha=0.4, markerscale=1)
    plt.savefig('bbr_zen_spn_total_flux.png', transparent=False)
    # plt.show()

def PLT_ZEN_SPN_DIFF():
    fname = 'data/bbr/20141002_C130_attcorr.txt'
    bbr = READ_BBR_V1(fname)

    fig = plt.figure(figsize=(8, 6))
    ax1 = fig.add_subplot(111)
    ax1.scatter(bbr.tmhr, bbr.zen_spn_diff_flux               , s=50, label='turn'           , alpha=0.3, facecolor='none', edgecolor='r', linewidth=0.8)
    ax1.scatter(bbr.tmhr, bbr.zen_spn_diff_flux_noturn        , s=50, label='no turn'        , alpha=0.3, facecolor='none', edgecolor='g', linewidth=0.8)
    ax1.set_xlim([25.0, 25.6])
    ax1.set_ylim([10,  60])
    ax1.set_xlabel('Time [hour]')
    ax1.set_ylabel('Flux [$\mathrm{W m^{-2}}$]')
    ax1.set_title('Zenith SPN Diffuse Flux')
    ax1.legend(loc='best', fontsize=12, framealpha=0.4, markerscale=1)
    plt.savefig('bbr_zen_spn_diff_flux.png', transparent=False)
    # plt.show()

def PLT_ANG_PIT():

    fnames = sorted(glob.glob('data/ssfr/*.plt2'))
    plt2   = READ_PLT2(fnames, dateRef=datetime.datetime(2014, 10, 2), secOffset=20.0)
    logic  = (plt2.tmhr_corr>=25.0)&(plt2.tmhr_corr<=25.6)

    fig = plt.figure(figsize=(8, 6))
    ax1 = fig.add_subplot(111)
    ax1.scatter(plt2.tmhr_corr[logic], plt2.ang_pit_a[logic]  , s=10, label='C130' , alpha=0.6, facecolor='none', edgecolor='r', linewidth=0.3)
    ax1.scatter(plt2.tmhr_corr[logic], plt2.ang_pit_m[logic], s=10, label='Motor', alpha=0.6, facecolor='none', edgecolor='g', linewidth=0.3)
    # ax1.scatter(plt2.tmhr_corr[logic], plt2.ang_pit[logic]-plt2.ang_pit_m[logic], s=10, label='C130-Motor', alpha=0.6, facecolor='none', edgecolor='gray', linewidth=0.3)
    ax1.axhline(0.0, ls='--')
    ax1.set_xlim([25.0, 25.6])
    ax1.set_ylim([3.0, 5.0])
    ax1.set_xlabel('Time [hour]')
    ax1.set_ylabel('Angle [$^\circ$]')
    ax1.set_title('Pitch Angle')
    ax1.legend(loc='best', fontsize=12, framealpha=0.4, markerscale=3)
    plt.savefig('ang-pit.png', transparent=True)
    plt.show()

def PLT_ANG_ROL():

    fnames = sorted(glob.glob('data/ssfr/*.plt2'))
    plt2   = READ_PLT2(fnames, dateRef=datetime.datetime(2014, 10, 2), secOffset=20.0)
    logic  = (plt2.tmhr_corr>=25.0)&(plt2.tmhr_corr<=25.6)

    fig = plt.figure(figsize=(8, 6))
    ax1 = fig.add_subplot(111)
    ax1.scatter(plt2.tmhr_corr[logic], plt2.ang_rol_a[logic], s=10, label='C130' , alpha=0.6, facecolor='none', edgecolor='r', linewidth=0.3)
    ax1.scatter(plt2.tmhr_corr[logic], plt2.ang_rol_m[logic], s=10, label='Motor', alpha=0.6, facecolor='none', edgecolor='g', linewidth=0.3)
    # ax1.scatter(plt2.tmhr_corr[logic], plt2.ang_rol[logic]-plt2.ang_rol_m[logic], s=10, label='C130-Motor', alpha=0.6, facecolor='none', edgecolor='gray', linewidth=0.3)
    ax1.axhline(0.0, ls='--')
    ax1.set_xlim([25.0, 25.6])
    ax1.set_ylim([-8, 4])
    ax1.set_xlabel('Time [hour]')
    ax1.set_ylabel('Angle [$^\circ$]')
    ax1.set_title('Roll Angle')
    ax1.legend(loc='best', fontsize=12, framealpha=0.4, markerscale=3)
    plt.savefig('ang-rol.png', transparent=True)

def PLT_ANG_HEAD():

    fnames = sorted(glob.glob('data/ssfr/*.plt2'))
    plt2   = READ_PLT2(fnames, dateRef=datetime.datetime(2014, 10, 2), secOffset=20.0)
    logic  = (plt2.tmhr_corr>=25.0)&(plt2.tmhr_corr<=25.6)

    fig = plt.figure(figsize=(8, 6))
    ax1 = fig.add_subplot(111)
    ax1.scatter(plt2.tmhr_corr[logic], plt2.ang_head[logic]  , s=10, label='C130' , alpha=0.6, facecolor='none', edgecolor='r', linewidth=0.3)
    # ax1.scatter(plt2.tmhr_corr[logic], plt2.ang_rol_m[logic], s=10, label='Motor', alpha=0.6, facecolor='none', edgecolor='g', linewidth=0.3)
    # ax1.scatter(plt2.tmhr_corr[logic], plt2.ang_rol[logic]-plt2.ang_rol_m[logic], s=10, label='C130-Motor', alpha=0.6, facecolor='none', edgecolor='gray', linewidth=0.3)
    ax1.axhline(0.0, ls='--')
    ax1.set_xlim([25.0, 25.6])
    # ax1.set_ylim([-8, 6])
    ax1.set_xlabel('Time [hour]')
    ax1.set_ylabel('Angle [$^\circ$]')
    ax1.set_title('Head Angle')
    ax1.legend(loc='best', fontsize=12, framealpha=0.4, markerscale=3)
    plt.savefig('ang-head.png', transparent=True)
    plt.show()

def PLT_AZA():

    fname = 'data/bbr/20141002_C130_attcorr.txt'
    bbr   = READ_BBR_V1(fname)
    f_theta = (bbr.zen_spn_total_flux_noturn - bbr.zen_spn_diff_flux_noturn) / bbr.zen_spn_total_flux_noturn

    fnames = sorted(glob.glob('data/ssfr/*.plt2'))
    plt2   = READ_PLT2(fnames, dateRef=datetime.datetime(2014, 10, 2), secOffset=20.0)
    logic  = (plt2.tmhr_corr>=25.0)&(plt2.tmhr_corr<=25.6)

    fig = plt.figure(figsize=(8, 6))
    ax1 = fig.add_subplot(111)
    ax1.scatter(plt2.tmhr_corr[logic], plt2.ang_head[logic]  , s=10, label='C130' , alpha=0.6, facecolor='none', edgecolor='r', linewidth=0.3)
    ax1.axhline(0.0, ls='--')
    ax1.set_xlim([25.0, 25.6])
    ax1.set_xlabel('Time [hour]')
    ax1.set_ylabel('Angle [$^\circ$]')
    ax1.set_title('Head Angle')
    ax1.legend(loc='best', fontsize=12, framealpha=0.4, markerscale=3)
    plt.savefig('ang-head.png', transparent=True)
    plt.show()

def PLT_OFFSET():

    fname = 'data/bbr/20141002_C130_attcorr.txt'
    bbr   = READ_BBR_V1(fname)
    zen_spn_direct_flux_noturn         = bbr.zen_spn_total_flux_noturn - bbr.zen_spn_diff_flux_noturn
    zen_spn_direct_flux_noturn_attcorr = bbr.zen_spn_total_flux_noturn_attcorr - bbr.zen_spn_diff_flux_noturn
    factor = zen_spn_direct_flux_noturn_attcorr/zen_spn_direct_flux_noturn

    fnames = sorted(glob.glob('data/ssfr/*.plt2'))
    dateRef = datetime.datetime(2014, 10, 2)
    plt2   = READ_PLT2(fnames, dateRef=dateRef, secOffset=20.0)
    logic  = (plt2.tmhr_corr>=25.0)&(plt2.tmhr_corr<=25.6)

    lon0 = np.interp(bbr.tmhr, plt2.tmhr_corr[logic], plt2.lon[logic])
    lat0 = np.interp(bbr.tmhr, plt2.tmhr_corr[logic], plt2.lat[logic])
    alt0 = np.interp(bbr.tmhr, plt2.tmhr_corr[logic], plt2.alt[logic])

    sza = np.zeros_like(lon0)
    for i in range(sza.size):
        dtime_i  = dateRef + datetime.timedelta(hours=bbr.tmhr[i])
        dtime_i  = dtime_i.replace(tzinfo=datetime.timezone.utc)
        sza[i] = 90.0 - pysolar.solar.get_altitude(lat0[i], lon0[i], dtime_i, elevation=alt0[i])

    cos_sza = np.cos(sza)

    fig = plt.figure(figsize=(8, 6))
    ax1 = fig.add_subplot(111)
    ax1.scatter(bbr.tmhr, np.arccos(cos_sza*factor), s=10, alpha=0.6, facecolor='none', edgecolor='k', linewidth=0.3)
    # ax1.scatter(bbr.tmhr, bbr.zen_spn_total_flux_noturn  , s=10, alpha=0.6, facecolor='none', edgecolor='k', linewidth=0.3)
    # ax1.axhline(0.0, ls='--')
    ax1.set_xlim([25.0, 25.6])
    ax1.set_ylim([0.0, 2.5])
    ax1.set_xlabel('Time [hour]')
    ax1.set_ylabel('Angle [$^\circ$]')
    ax1.set_title('Offset Angle')
    # ax1.legend(loc='best', fontsize=12, framealpha=0.4, markerscale=3)
    plt.savefig('ang-offset.png', transparent=True)
    plt.show()

if __name__ == '__main__':
    import matplotlib as mpl
    import matplotlib.pyplot as plt
    from matplotlib.ticker import FixedLocator
    from mpl_toolkits.basemap import Basemap
    from pre_ssfr import READ_PLT2


    # PLT_ANG_PIT()
    # PLT_ANG_ROL()
    # PLT_ANG_HEAD()
    # PLT_AZA()
    # PLT_OFFSET()
    # exit()

    PLT_ZEN_SW()
    exit()
    PLT_NAD_SW()
    PLT_ZEN_SPN_TOTAL()
    PLT_ZEN_SPN_DIFF()
