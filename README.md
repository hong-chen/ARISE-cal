# ARISE-cal

Tested on macOS v10.12.3 with

- Python v3.6.0 :: Anaconda 4.3.1 (x86_64)

  - os
  - glob
  - h5py
  - struct
  - numpy
  - datetime
  - scipy

Additional packages:

- [pysolar](http://pysolar.org) (`pip install pysolar`)


--------

File: `pre_ssfr.py`

Functions:

  - `READ_SKS_ONE_V1`
  - `READ_SKS`

--------
