import os
import glob
import h5py
import struct
import numpy as np
import datetime
from scipy.io import readsav
from scipy import stats
import pysolar
from pre_hsk import READ_HSK

def PRH2ZA(ang_pit, ang_rol, ang_head, is_rad=False):

    """
    input:
    ang_pit   (Pitch)   [deg]: positive (+) values indicate nose up
    ang_rol   (Roll)    [deg]: positive (+) values indicate right wing down
    ang_head  (Heading) [deg]: positive (+) values clockwise, w.r.t. north

    "vec": normal vector of the surface of the sensor

    return:
    ang_zenith : angle of "vec" [deg]
    ang_azimuth: angle of "vec" [deg]: positive (+) values clockwise, w.r.t. north
    """

    if not is_rad:
        rad_pit  = np.deg2rad(ang_pit)
        rad_rol  = np.deg2rad(ang_rol)
        rad_head = np.deg2rad(ang_head)

    uz =  np.cos(rad_rol)*np.cos(rad_pit)
    ux =  np.sin(rad_rol)
    uy = -np.cos(rad_rol)*np.sin(rad_pit)

    vz = uz
    vx = ux*np.cos(rad_head) + uy*np.sin(rad_head)
    vy = uy*np.cos(rad_head) - ux*np.sin(rad_head)

    ang_zenith  = np.rad2deg(np.arccos(vz))
    ang_azimuth = np.rad2deg(np.arctan2(vx,vy))

    return ang_zenith, ang_azimuth

def MUSLOPE(sza, saa, iza, iaa, is_rad=False):

    if not is_rad:
        rad_sza = np.deg2rad(sza)
        rad_saa = np.deg2rad(saa)
        rad_iza = np.deg2rad(iza)
        rad_iaa = np.deg2rad(iaa)

    zs = np.cos(rad_sza)
    ys = np.sin(rad_sza) * np.cos(rad_saa)
    xs = np.sin(rad_sza) * np.sin(rad_saa)

    zi = np.cos(rad_iza)
    yi = np.sin(rad_iza) * np.cos(rad_iaa)
    xi = np.sin(rad_iza) * np.sin(rad_iaa)

    mu = xs*xi + ys*yi + zs*zi

    return mu

# +++++++++++++++++++++++++    for .SKS files   ++++++++++++++++++++++++++++++
class READ_SKS_TEST:

    def __init__(self, fnames, dateRef=None, Ndata=600, secOffset=0.0, verbose=False):

        f = h5py.File('test_cir2_raw.h5', 'r')
        self.spectra           = f['spectra'][...]
        self.shutter           = f['shutter'][...]
        self.int_time          = f['int_time'][...]
        self.temp              = f['temp'][...]
        self.jday_NSF          = f['jday_NSF'][...]
        self.jday_cRIO         = f['jday_cRIO'][...]
        self.qual_flag         = f['qual_flag'][...]
        self.jday_corr         = f['jday_corr'][...]
        self.tmhr_corr         = f['tmhr_corr'][...]
        self.spectra_dark_corr = f['spectra_dark_corr'][...]
        f.close()

        f = h5py.File('test_cir2_lrt.h5', 'r')
        f.close()

        if dateRef == None:
            jdayRef = int(self.jday_NSF[30])
            self.dateRef = datetime.datetime(1, 1, 1)+datetime.timedelta(seconds=(jdayRef-1.0)*86400.0)
        else:
            jdayRef = (dateRef -(datetime.datetime(1, 1, 1))).days + 1.0
            self.dateRef = dateRef

        if verbose:
            print('----------------------------------------------------------')

        if verbose:
            print('+++++++++++++++++++Starting correction++++++++++++++++++++')
            print('Dark correction...')

        if verbose:
            print('Non-linearity correction...')
        self.spectra_nlin_corr = self.spectra_dark_corr.copy()
        fname_nlin ='data/ssfr/aux/20141121.sav'
        Nsen       = 1
        self.NLIN_CORR(fname_nlin, Nsen)
        fname_nlin ='data/ssfr/aux/20141119.sav'
        Nsen       = 3
        self.NLIN_CORR(fname_nlin, Nsen)

        # if verbose:
        #     print('Cosine correction...')
        fnames    = sorted(glob.glob('data/ssfr/*.plt2'))
        # data_plt  = READ_PLT2(fnames, dateRef=self.dateRef, secOffset=16.1)
        data_plt  = READ_PLT2(fnames, dateRef=self.dateRef, secOffset=20.0)
        self.COS_CORR(data_plt)
        self.COUNT2FLUX(self.spectra_nlin_corr)


        index = np.argmin(np.abs(self.wvl_zen-666.0))
        self.flux_ori      = self.spectra_flux_zen[:, index]
        self.flux_corr     = self.flux_ori * self.cos_corr_factor
        self.flux_corr_ori = self.flux_ori * self.cos_corr_factor_ori

        self.cos_corr_factor0 = self.cos_corr_factor[index]
        self.cos_corr_factor_ori0 = self.cos_corr_factor[index]

        if verbose:
            print('----------------------------------------------------------')

    def DARK_CORR(self, mode=-1, darkExtend=2, lightExtend=2, countOffset=0, lightThr=10, darkThr=5):

        if self.shutter[0] == 0:
            darkL = np.array([], dtype=np.int32)
            darkR = np.array([0], dtype=np.int32)
        else:
            darkR = np.array([], dtype=np.int32)
            darkL = np.array([0], dtype=np.int32)

        darkL0 = np.squeeze(np.argwhere((self.shutter[1:]-self.shutter[:-1]) ==  1)) + 1
        darkL  = np.hstack((darkL, darkL0))

        darkR0 = np.squeeze(np.argwhere((self.shutter[1:]-self.shutter[:-1]) == -1)) + 1
        darkR  = np.hstack((darkR, darkR0))

        if self.shutter[-1] == 0:
            darkL = np.hstack((darkL, self.shutter.size))
        else:
            darkR = np.hstack((darkR, self.shutter.size))

        self.spectra_dark_corr = self.spectra.copy() + countOffset
        self.dark_offset       = np.zeros(self.spectra.shape, dtype=np.float64)
        self.dark_std          = np.zeros(self.spectra.shape, dtype=np.float64)

        Nrecord, Nchannel, Nsensor = self.spectra.shape
        if mode == -1:
            if darkL.size-darkR.size==0:
                if darkL[0]>darkR[0] and darkL[-1]>darkR[-1]:
                    darkL = darkL[:-1]
                    darkR = darkR[1:]
            elif darkL.size-darkR.size==1:
                if darkL[0]>darkR[0] and darkL[-1]<darkR[-1]:
                    darkL = darkL[1:]
                elif darkL[0]<darkR[0] and darkL[-1]>darkR[-1]:
                    darkL = darkL[:-1]
            elif darkR.size-darkL.size==1:
                if darkL[0]>darkR[0] and darkL[-1]<darkR[-1]:
                    darkR = darkR[1:]
                elif darkL[0]<darkR[0] and darkL[-1]>darkR[-1]:
                    darkR = darkR[:-1]
            else:
                exit('Error [READ_SKS.DARK_CORR]: darkL and darkR are wrong.')

            for i in range(darkL.size-1):
                if darkR[i] < darkL[i]:
                    exit('Error [READ_SKS.DARK_CORR]: darkL > darkR.')

                darkLL = darkL[i] + darkExtend
                darkLR = darkR[i] - darkExtend
                darkRL = darkL[i+1] + darkExtend
                darkRR = darkR[i+1] - darkExtend

                if i == 0:
                    self.shutter[:darkLL] = -1  # omit the data before the first dark cycle

                lightL = darkR[i]   + lightExtend
                lightR = darkL[i+1] - lightExtend

                if lightR-lightL>lightThr and darkLR-darkLL>darkThr and darkRR-darkRL>darkThr:

                    self.shutter[darkL[i]:darkLL] = -1
                    self.shutter[darkLR:darkR[i]] = -1
                    self.shutter[darkR[i]:lightL] = -1
                    self.shutter[lightR:darkL[i+1]] = -1
                    self.shutter[darkL[i+1]:darkRL] = -1
                    self.shutter[darkRR:darkR[i+1]] = -1

                    int_dark  = np.append(self.int_time[darkLL:darkLR], self.int_time[darkRL:darkRR]).mean()
                    int_light = self.int_time[lightL:lightR].mean()

                    if np.abs(int_dark - int_light) > 0.0001:
                        self.shutter[lightL:lightR] = -1
                    else:
                        interp_x  = np.append(self.tmhr[darkLL:darkLR], self.tmhr[darkRL:darkRR])
                        if i==darkL.size-2:
                            target_x  = self.tmhr[darkLL:darkRR]
                        else:
                            target_x  = self.tmhr[darkLL:darkRL]

                        for ichan in range(Nchannel):
                            for isen in range(Nsensor):
                                interp_y = np.append(self.spectra[darkLL:darkLR,ichan,isen], self.spectra[darkRL:darkRR,ichan,isen])
                                slope, intercept, r_value, p_value, std_err  = stats.linregress(interp_x, interp_y)
                                if i==darkL.size-2:
                                    self.dark_offset[darkLL:darkRR, ichan, isen] = target_x*slope + intercept
                                    self.spectra_dark_corr[darkLL:darkRR, ichan, isen] -= self.dark_offset[darkLL:darkRR, ichan, isen]
                                    self.dark_std[darkLL:darkRR, ichan, isen] = np.std(interp_y)
                                else:
                                    self.dark_offset[darkLL:darkRL, ichan, isen] = target_x*slope + intercept
                                    self.spectra_dark_corr[darkLL:darkRL, ichan, isen] -= self.dark_offset[darkLL:darkRL, ichan, isen]
                                    self.dark_std[darkLL:darkRL, ichan, isen] = np.std(interp_y)

                else:
                    self.shutter[darkL[i]:darkR[i+1]] = -1

            self.shutter[darkRR:] = -1  # omit the data after the last dark cycle

        elif mode == -2:
            print('Message [DARK_CORR]: Not implemented...')

        elif mode == -3:

            #if darkL.size-darkR.size==0:
                #if darkL[0]>darkR[0] and darkL[-1]>darkR[-1]:
                    #darkL = darkL[:-1]
                    #darkR = darkR[1:]
            #elif darkL.size-darkR.size==1:
                #if darkL[0]>darkR[0] and darkL[-1]<darkR[-1]:
                    #darkL = darkL[1:]
                #elif darkL[0]<darkR[0] and darkL[-1]>darkR[-1]:
                    #darkL = darkL[:-1]
            #elif darkR.size-darkL.size==1:
                #if darkL[0]>darkR[0] and darkL[-1]<darkR[-1]:
                    #darkR = darkR[1:]
                #elif darkL[0]<darkR[0] and darkL[-1]>darkR[-1]:
                    #darkR = darkR[:-1]
            #else:
                #exit('Error [READ_SKS.DARK_CORR]: darkL and darkR are wrong.')

            for i in range(darkR.size):
                darkLL = darkL[i]   + darkExtend
                darkLR = darkR[i]   - darkExtend
                lightL = darkR[i]   + lightExtend
                lightR = darkL[i+1] - lightExtend

                self.shutter[darkL[i]:darkLL] = -1
                self.shutter[darkLR:darkR[i]] = -1
                self.shutter[darkR[i]:lightL] = -1
                self.shutter[lightR:darkL[i+1]] = -1

                int_dark  = self.int_time[darkLL:darkLR].mean()
                int_light = self.int_time[lightL:lightR].mean()
                if np.abs(int_dark - int_light) > 0.0001:
                    self.shutter[lightL:lightR] = -1
                    exit('Error [READ_SKS.DARK_CORR]: inconsistent integration time.')
                else:
                    for itmhr in range(darkLR, lightR):
                        for isen in range(Nsensor):
                            dark_offset0 = np.mean(self.spectra[darkLL:darkLR, :, isen], axis=0)
                            self.dark_offset[itmhr, :, isen] = dark_offset0
                    self.spectra_dark_corr[lightL:lightR,:,:] -= self.dark_offset[lightL:lightR,:,:]

        elif mode == -4:
            print('Message [DARK_CORR]: Not implemented...')

    def NLIN_CORR(self, fname_nlin, Nsen, verbose=False):

        int_time0 = np.mean(self.int_time[:, Nsen])
        f_nlin = readsav(fname_nlin)

        if abs(f_nlin.iin_-int_time0)>1.0e-5:
            exit('Error [READ_SKS]: Integration time do not match.')

        for iwvl in range(256):
            xx0   = self.spectra_nlin_corr[:,iwvl,Nsen].copy()
            xx    = np.zeros_like(xx0)
            yy    = np.zeros_like(xx0)
            #self.spectra_nlin_corr[:,iwvl,Nsen] = np.nan
            self.spectra_nlin_corr[:,iwvl,Nsen] = -1.0
            logic_xx     = (xx0>-100)
            if verbose:
                print('++++++++++++++++++++++++++++++++++++++++++++++++')
                print('range', f_nlin.mxm_[0,iwvl]*f_nlin.in_[iwvl], f_nlin.mxm_[1,iwvl]*f_nlin.in_[iwvl])
                print('good', logic_xx.sum(), xx0.size)
            xx0[logic_xx] = xx0[logic_xx]/f_nlin.in_[iwvl]

            if (f_nlin.bad_[1,iwvl]<1.0) and (f_nlin.mxm_[0,iwvl]>=1.0e-3):

                #+ data in range (0, minimum)
                yy_e = 0.0
                for ideg in range(f_nlin.gr_):
                    yy_e += f_nlin.res2_[ideg,iwvl]*f_nlin.mxm_[0,iwvl]**ideg
                slope = yy_e/f_nlin.mxm_[0,iwvl]
                logic_xx     = (xx0>-100) & (xx0<f_nlin.mxm_[0,iwvl])
                if verbose:
                    print('0-min', logic_xx.sum(), xx0.size)
                    print('data', xx0[logic_xx])
                xx[xx<0]     = 0.0
                xx[logic_xx] = xx0[logic_xx]
                yy[logic_xx] = xx[logic_xx]*slope

                self.spectra_nlin_corr[logic_xx,iwvl,Nsen] = yy[logic_xx]*f_nlin.in_[iwvl]
                #-

                #+ data in range [minimum, maximum]
                logic_xx     = (xx0>=f_nlin.mxm_[0,iwvl]) & (xx0<=f_nlin.mxm_[1,iwvl])
                xx[logic_xx] = xx0[logic_xx]
                if verbose:
                    print('min-max', logic_xx.sum(), xx0.size)
                    print('------------------------------------------------')
                for ideg in range(f_nlin.gr_):
                    yy[logic_xx] += f_nlin.res2_[ideg, iwvl]*xx[logic_xx]**ideg

                self.spectra_nlin_corr[logic_xx,iwvl,Nsen] = yy[logic_xx]*f_nlin.in_[iwvl]
                #-

    def COS_CORR(self, data_plt,
                 fname_zen = 'data/ssfr/aux/20150221_LCx_zenith.out',
                 fname_nad = 'data/ssfr/aux/20150221_LCx_nadir.out'):

        # iza, iaa = PRH2ZA(data_plt.ang_pit_a-data_plt.ang_pit_m, data_plt.ang_rol_a-data_plt.ang_rol_m, data_plt.ang_head)
        # iza, iaa = PRH2ZA(data_plt.ang_pit_a, data_plt.ang_rol_a, data_plt.ang_head)
        # offset_angle = 2.0
        offset_angle = 0.7
        iza, iaa = PRH2ZA(data_plt.ang_pit_a-offset_angle, data_plt.ang_rol_a, data_plt.ang_head)

        tmhrMin = self.tmhr_corr.min()
        tmhrMax = self.tmhr_corr.max()

        if data_plt.tmhr_corr.min() > tmhrMin or data_plt.tmhr_corr.max() < tmhrMax:
            exit('Error [READ_SKS.COS_CORR]: GPS time range from INS cannot include SSFR time range to be interpolated.')

        from pre_bbr import READ_BBR_V1
        fname = 'data/bbr/20141002_C130_attcorr.txt'
        bbr = READ_BBR_V1(fname)

        fraction = bbr.zen_spn_diff_flux_noturn/bbr.zen_spn_total_flux_noturn

        # logic = (data_plt.tmhr_corr>=tmhrMin) & (data_plt.tmhr_corr<=tmhrMax) & (np.abs(data_plt.ang_rol_a-data_plt.ang_rol_m)<1.5)
        logic = (data_plt.tmhr_corr>=tmhrMin) & (data_plt.tmhr_corr<=tmhrMax)

        # iza0  = np.interp(self.tmhr_corr, data_plt.tmhr_corr[logic], iza[logic])
        # iaa0  = np.interp(self.tmhr_corr, data_plt.tmhr_corr[logic], iaa[logic])

        # lon0  = np.interp(self.tmhr_corr, data_plt.tmhr_corr[logic], data_plt.lon[logic])
        # lat0  = np.interp(self.tmhr_corr, data_plt.tmhr_corr[logic], data_plt.lat[logic])
        # alt0  = np.interp(self.tmhr_corr, data_plt.tmhr_corr[logic], data_plt.alt[logic])

        iza0  = np.interp(bbr.tmhr, data_plt.tmhr_corr[logic], iza[logic])
        iaa0  = np.interp(bbr.tmhr, data_plt.tmhr_corr[logic], iaa[logic])

        lon0  = np.interp(bbr.tmhr, data_plt.tmhr_corr[logic], data_plt.lon[logic])
        lat0  = np.interp(bbr.tmhr, data_plt.tmhr_corr[logic], data_plt.lat[logic])
        alt0  = np.interp(bbr.tmhr, data_plt.tmhr_corr[logic], data_plt.alt[logic])

        # calculate solar zenith angle (sza), solar azimuth angle (saa)
        # sza0 = np.zeros_like(self.tmhr_corr)
        # saa0 = np.zeros_like(self.tmhr_corr)
        sza0 = np.zeros_like(bbr.tmhr)
        saa0 = np.zeros_like(bbr.tmhr)
        for i in range(lon0.size):
            dtime_i  = self.dateRef + datetime.timedelta(hours=self.tmhr_corr[i])
            dtime_i  = dtime_i.replace(tzinfo=datetime.timezone.utc)
            sza0[i] = 90.0 - pysolar.solar.get_altitude(lat0[i], lon0[i], dtime_i, elevation=alt0[i])
            saa_i = pysolar.solar.get_azimuth(lat0[i], lon0[i], dtime_i, elevation=alt0[i])
            if saa_i >= 0.0:
                if 0.0<=saa_i<=180.0:
                    saa_i = 180.0 - saa_i
                elif 180.0<saa_i<=360.0:
                    saa_i = 540.0 - saa_i
                else:
                    print('Warning [READ_SKS.COS_CORR]: invalid solar azimuth angle.')
            elif saa_i < 0.0:
                if -180.0<=saa_i<0.0:
                    saa_i = -saa_i + 180.0
                elif -360.0<=saa_i<-180.0:
                    saa_i = -saa_i - 180.0
                else:
                    print('Warning [READ_SKS.COS_CORR]: invalid solar azimuth angle.')
            saa0[i] = saa_i

        self.sza0 = sza0
        self.saa0 = saa0
        self.alt0 = alt0

        dc       = MUSLOPE(sza0, saa0, iza0, iaa0)
        self.dc  = dc

        mu       = np.cos(np.deg2rad(sza0))
        factor   = mu/dc

        f = h5py.File('test_cir2_lrt.h5', 'r')
        flux_lrt = f['flux_lrt'][...]
        wvl_lrt  = f['wvl_lrt'][...]
        f.close()

        wvlRange = [200, 4000]
        indices  = (wvl_lrt>=wvlRange[0])&(wvl_lrt<=wvlRange[1])
        wvl      = wvl_lrt[indices]

        bbr_lrt  = np.zeros(flux_lrt.shape[0], dtype=np.float64)
        for i in range(flux_lrt.shape[0]):
            flux       = flux_lrt[i, :][indices]
            bbr_lrt[i] = np.trapz(flux, x=wvl)

        fig = plt.figure(figsize=(8, 6))
        ax1 = fig.add_subplot(111)
        # ax1.scatter(bbr.tmhr, bbr.zen_sw_flux               , s=50, label='turn'           , alpha=0.3, facecolor='none', edgecolor='r', linewidth=0.8)
        # ax1.scatter(bbr.tmhr, bbr.zen_sw_flux_noturn        , s=50, label='no turn'        , alpha=0.3, facecolor='none', edgecolor='b', linewidth=0.8)
        # ax1.scatter(bbr.tmhr, mu*1100.0, s=50, label='$\mu$'    , alpha=0.3, facecolor='none', edgecolor='k', linewidth=0.8)
        ax1.scatter(self.tmhr_corr, bbr_lrt, s=50, label='libRadtran'    , alpha=0.3, facecolor='none', edgecolor='k', linewidth=0.8)
        # ax1.scatter(bbr.tmhr, 0.2*bbr.zen_sw_flux_noturn+0.8*bbr.zen_sw_flux_noturn*factor  , s=50, label='Hong'        , alpha=0.3, facecolor='none', edgecolor='r', linewidth=0.8)
        ax1.scatter(bbr.tmhr, fraction*bbr.zen_sw_flux_noturn+(1.0-fraction)*bbr.zen_sw_flux_noturn*factor  , s=50, label='Hong'        , alpha=0.3, facecolor='none', edgecolor='r', linewidth=0.8)
        ax1.scatter(bbr.tmhr, bbr.zen_sw_flux_noturn_attcorr , s=50, label='Anthony'     , alpha=0.3, facecolor='none', edgecolor='b', linewidth=0.8)
        ax1.set_xlim([25.0, 25.6])
        ax1.set_ylim([200,  600])
        ax1.set_xlabel('Time [hour]')
        ax1.set_ylabel('Flux [$\mathrm{W m^{-2}}$]')
        ax1.set_title('Zenith Shortwave Flux')
        ax1.legend(loc='best', fontsize=14, framealpha=0.4, markerscale=1)
        plt.savefig('comp_%3.1f.png' % offset_angle)
        plt.show()
        exit()

        # 1. data processing
        # 2. anothoy's presentation
        # 3. modis flux

        fig = plt.figure(figsize=(8, 6))
        ax1 = fig.add_subplot(111)
        ax1.set_xlim((25.0, 25.6))
        # ax1.set_xlabel('Time [hour]')
        # ax1.set_ylabel('Angle [$\mathrm{^\circ}$]')

        # ax1.scatter(self.tmhr_corr, sza0)
        # ax1.set_ylim((68, 72))
        # ax1.set_title('Solar Zenith Angle')
        # plt.savefig('cir2-sza.png')

        # ax1.scatter(self.tmhr_corr, saa0)
        # ax1.set_ylim((218, 230))
        # ax1.set_title('Solar Azimuth Angle')
        # plt.savefig('cir2-saa.png')

        # ax1.scatter(self.tmhr_corr, iza0)
        # ax1.set_ylim((0, 8))
        # ax1.set_title('Aircraft Zenith Angle')
        # plt.savefig('cir2-iza.png')

        # ax1.scatter(self.tmhr_corr, iaa0)
        # ax1.set_ylim((-180, 180))
        # ax1.set_title('Aircraft Azimuth Angle')
        # plt.savefig('cir2-iaa.png')

        ax1.scatter(self.tmhr_corr, dc*1200.0)
        # ax1.set_ylim((0.2, 0.5))
        ax1.set_ylim((200, 600))
        ax1.axis('off')
        # ax1.set_title('Slope $\mu \\times 1200$')
        plt.savefig('cir2-dc.png')
        # plt.show()
        exit()

        # ax1.scatter(self.tmhr_corr, np.arccos(dc))
        # ax1.set_ylim((1.1, 1.4))
        # ax1.set_title('Angle between Sensor Surface and Solar Beam')
        # plt.savefig('cir2-offset.png')

        indice = np.int_(np.round(dc, decimals=3)*1000.0)
        indice[indice>1000]=1000
        indice[indice<0]   =0

        f_cos = readsav(fname_zen)
        wvl   = f_cos.wvlz
        resp  = f_cos.res_z_wvl
        mu_i  = f_cos.mu_interpol

        wvl0   = 666.0
        index0 = np.argmin(np.abs(wvl-wvl0))
        resp0     = resp[index0, :] - 0.008
        resp0_ori = resp[index0, :]
        #resp0  = resp[index0, :]
        #resp0  = resp[index0, :] + 0.013
        resp0[resp0<0.0000001] = 0.0000001
        resp0_ori[resp0_ori<0.0000001] = 0.0000001

        self.cos_corr_factor     = np.cos(np.deg2rad(sza0)) / resp0[indice]
        self.cos_corr_factor_ori = np.cos(np.deg2rad(sza0)) / resp0_ori[indice]

    def COUNT2FLUX(self, countIn,
                   fname_ns = 'data/ssfr/aux/20140924_s1n_150B_s300.sav',
                   fname_ni = 'data/ssfr/aux/20140924_s1n_150B_i300.sav',
                   fname_zs = 'data/ssfr/aux/20140921_s1z_150B_s300.sav',
                   fname_zi = 'data/ssfr/aux/20140921_s1z_150B_i300.sav'
            ):
        """
        Convert digital count to flux (irradiance)
        """

        f_ns = readsav(fname_ns)
        f_ni = readsav(fname_ni)
        f_zs = readsav(fname_zs)
        f_zi = readsav(fname_zi)

        logic_nsi2 = (f_ns.wl_si2 <= f_ni.join)
        logic_nin2 = (f_ni.wl_in2 >= f_ni.join)
        nsi2 = logic_nsi2.sum()
        nin2 = logic_nin2.sum()
        n2 = nsi2 + nin2

        logic_nsi1 = (f_zs.wl_si1 <= f_ni.join)
        logic_nin1 = (f_zi.wl_in1 >= f_ni.join)
        nsi1 = logic_nsi1.sum()
        nin1 = logic_nin1.sum()
        n1 = nsi1 + nin1

        f_ns.resp2_si2[f_ns.resp2_si2<1.0e-10] = -1
        f_ni.resp2_in2[f_ni.resp2_in2<1.0e-10] = -1
        f_zs.resp2_si1[f_zs.resp2_si1<1.0e-10] = -1
        f_zi.resp2_in1[f_zi.resp2_in1<1.0e-10] = -1

        self.wvl_zen = np.append(f_zs.wl_si1[logic_nsi1], f_zi.wl_in1[logic_nin1][::-1])
        self.wvl_nad = np.append(f_ns.wl_si2[logic_nsi2], f_ni.wl_in2[logic_nin2][::-1])
        self.spectra_flux_zen = np.zeros((self.tmhr_corr.size, n1), dtype=np.float64)
        self.spectra_flux_nad = np.zeros((self.tmhr_corr.size, n2), dtype=np.float64)
        for i in range(self.tmhr_corr.size):
            if self.shutter[i] == 0:
                self.spectra_flux_zen[i, :nsi1] = countIn[i, logic_nsi1, 0]/float(self.int_time[i, 0])/f_zs.resp2_si1[logic_nsi1]
                self.spectra_flux_zen[i, nsi1:] = (countIn[i, logic_nin1, 1]/float(self.int_time[i, 1])/f_zi.resp2_in1[logic_nin1])[::-1]
                self.spectra_flux_nad[i, :nsi2] = countIn[i, logic_nsi2, 2]/float(self.int_time[i, 2])/f_ns.resp2_si2[logic_nsi2]
                self.spectra_flux_nad[i, nsi2:] = (countIn[i, logic_nin2, 3]/float(self.int_time[i, 3])/f_ni.resp2_in2[logic_nin2])[::-1]
            else:
                self.spectra_flux_zen[i, :] = -1.0
                self.spectra_flux_nad[i, :] = -1.0

        self.spectra_flux_zen[self.spectra_flux_zen<0.0] = np.nan
        self.spectra_flux_nad[self.spectra_flux_nad<0.0] = np.nan
# ----------------------------------------------------------------------------

# ++++++++++++++++++++++++    for .plt2 files   ++++++++++++++++++++++++++++++
def READ_PLT2_ONE_V1(fname, vnames=None, dataLen=126, verbose=False):

    fileSize = os.path.getsize(fname)
    if fileSize > dataLen:
        iterN    = fileSize // dataLen
        residual = fileSize%  dataLen
        if residual != 0:
            print('Warning [READ_PLT2_V2]: %s has invalid data size.' % fname)
    else:
        exit('Error [READ_PLT2_V2]: %s has invalid file size.' % fname)

    vnames_dict  = {
                   'mini_pit':0,
                   'mini_rol':1,
                 'GPSSeconds':2,
                    'GPSWeek':3,
                        'Lat':4,
                        'Lon':5,
                        'Alt':6,
                        'Rol':7,
                        'Pit':8,
                        'Azi':9,
             'North_velocity':10,
              'East_velocity':11,
                'Up_velocity':12,
                  'motor_Rol':13,
                  'motor_Pit':14,
                 'temp_stage':15,
               'temp_c_pitch':16,
                 'temp_c_rol':17,
               'temp_m_pitch':18,
                 'temp_m_rol':19,
                      'acc_x':20,
                      'acc_y':21,
                      'acc_z':22,
          'relative_humidity':23,
        'chassis_temperature':24,
             'system_voltage':25,
      'INS_solution_inactive':26,
               'INS_aligning':27,
      'INS_solution_not_good':28,
          'INS_solution_good':29,
      'INS_bad_GPS_agreement':30,
     'INS_alignment_complete':31,
               'power_status':32,
            'SPAN_CPT_status':33,
                'file_status':34,
               'pitch_status':35,
                'roll_status':36,
                'FPGA_status':37,
                  'ARINC_rol':38,
                  'ARINC_pit':39,
                  'data_flag':40,
            'mini_INS_status':41
                  }

    if vnames == None:
        dataAll = np.zeros((iterN, len(vnames_dict)), dtype=np.float64)
        print('Warning [READ_PLT2_V2]: some variables should be integer type, please change accordingly.')
    else:
        dataAll = np.zeros((iterN, len(vnames)), dtype=np.float64)
        vnames_indice = []
        for vname in vnames:
            if vnames_dict[vname] in [26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 40, 41]:
                print('Warning [READ_PLT2_V2]: %s should be integer type, please change accordingly.' % vname)
            vnames_indice.append(vnames_dict[vname])

    f = open(fname, 'rb')
    if verbose:
        print('++++++++++++++++++++++++++++++++++++++++++++++++++')
        print('Reading %s...' % fname.split('/')[-1])

    # read data record
    for i in range(iterN):
        dataRec = f.read(dataLen)
        data    = struct.unpack('<26f12B2f2B', dataRec)
        if vnames == None:
            dataAll[i,:] = np.array(data, dtype=np.float64)
        else:
            dataAll[i,:] = np.array(data, dtype=np.float64)[vnames_indice]

    if verbose:
        print(dataAll[:, 0].min()/86400.0, dataAll[:, 0].max()/86400.0)
        print('--------------------------------------------------')

    return dataAll

class READ_PLT2:
    def __init__(self, fnames, dateRef=None, Ndata=15000, config=None, secOffset=0.0):

        if type(fnames) is not list:
            exit('Error [READ_PLT2]: input variable "fnames" should be in list type.')

        vnames=['GPSSeconds', 'GPSWeek', 'Pit', 'Rol', 'motor_Pit', 'motor_Rol', 'Azi', 'Lon', 'Lat', 'Alt', 'ARINC_pit', 'ARINC_rol']
        Nx         = Ndata * len(fnames)
        dataAll    = np.zeros((Nx, len(vnames)), dtype=np.float64)

        Nstart = 0
        for fname in fnames:
            dataAll0 = READ_PLT2_ONE_V1(fname, vnames=vnames, dataLen=126, verbose=False)
            Nend = Nstart + dataAll0.shape[0]
            dataAll[Nstart:Nend, ...]  = dataAll0
            Nstart = Nend

        if config != None:
            self.config = config

        dataAll   = dataAll[:Nend, ...]

        date0   = datetime.datetime(1980, 1, 6)
        jday0   = (date0   -(datetime.datetime(1, 1, 1))).days + 1.0

        self.jday = dataAll[:, 0]/86400.0 + 7.0* dataAll[:, 1] + jday0
        if dateRef == None:
            jdayRef = int(self.jday[0])
        else:
            jdayRef = (dateRef -(datetime.datetime(1, 1, 1))).days + 1.0

        self.tmhr = (self.jday-jdayRef)*24.0

        self.secOffset = secOffset
        self.jday_corr = self.jday - secOffset/86400.0
        self.tmhr_corr = self.tmhr - secOffset/3600.0

        self.ang_pit     = dataAll[:, 2] # light collector pitch/roll angle
        self.ang_rol     = dataAll[:, 3]
        self.ang_pit_m   = dataAll[:, 4] # motor pitch/roll angle
        self.ang_rol_m   = dataAll[:, 5]
        self.ang_head360 = dataAll[:, 6] # heading angle
        self.lon         = dataAll[:, 7]
        self.lat         = dataAll[:, 8]
        self.alt         = dataAll[:, 9]
        self.ang_pit_a   = dataAll[:, 10]# aircraft pitch/roll angle
        self.ang_rol_a   = dataAll[:, 11]

        self.ang_head    = self.ang_head360.copy()
        self.ang_head[self.ang_head>180.0] -= 360.0
# ----------------------------------------------------------------------------

def BBR_COS_CORR(data_plt):

    from pre_bbr import READ_BBR_V1
    fname = 'data/bbr/20141002_C130_attcorr.txt'
    bbr = READ_BBR_V1(fname)

    p_offset = 0.7
    iza, iaa = PRH2ZA(data_plt.ang_pit_a-p_offset, data_plt.ang_rol_a, data_plt.ang_head)

    diff_ratio   = bbr.zen_spn_diff_flux_noturn/bbr.zen_spn_total_flux_noturn

    logic = (data_plt.tmhr_corr>=25.00) & (data_plt.tmhr_corr<=25.60)

    iza0  = np.interp(bbr.tmhr, data_plt.tmhr_corr[logic], iza[logic])
    iaa0  = np.interp(bbr.tmhr, data_plt.tmhr_corr[logic], iaa[logic])

    lon0  = np.interp(bbr.tmhr, data_plt.tmhr_corr[logic], data_plt.lon[logic])
    lat0  = np.interp(bbr.tmhr, data_plt.tmhr_corr[logic], data_plt.lat[logic])
    alt0  = np.interp(bbr.tmhr, data_plt.tmhr_corr[logic], data_plt.alt[logic])

    sza0 = np.zeros_like(bbr.tmhr)
    saa0 = np.zeros_like(bbr.tmhr)
    for i in range(lon0.size):
        dtime_i  = datetime.datetime(2014, 10, 2) + datetime.timedelta(hours=bbr.tmhr[i])
        dtime_i  = dtime_i.replace(tzinfo=datetime.timezone.utc)
        sza0[i]  = 90.0 - pysolar.solar.get_altitude(lat0[i], lon0[i], dtime_i, elevation=alt0[i])
        saa_i    = pysolar.solar.get_azimuth(lat0[i], lon0[i], dtime_i, elevation=alt0[i])
        if saa_i >= 0.0:
            if 0.0<=saa_i<=180.0:
                saa_i = 180.0 - saa_i
            elif 180.0<saa_i<=360.0:
                saa_i = 540.0 - saa_i
            else:
                print('Warning [BBR_COS_CORR]: invalid solar azimuth angle.')
        elif saa_i < 0.0:
            if -180.0<=saa_i<0.0:
                saa_i = -saa_i + 180.0
            elif -360.0<=saa_i<-180.0:
                saa_i = -saa_i - 180.0
            else:
                print('Warning [BBR_COS_CORR]: invalid solar azimuth angle.')
        saa0[i] = saa_i

    dc       = MUSLOPE(sza0, saa0, iza0, iaa0)

    mu       = np.cos(np.deg2rad(sza0))
    factor   = mu/dc

    from pre_ssfr import READ_SKS_TEST

    fnames = sorted(glob.glob('data/ssfr/*.SKS'))

    sks    = READ_SKS_TEST(fnames, dateRef=datetime.datetime(2014, 10, 2), verbose=True)

    f = h5py.File('test_cir2_lrt.h5', 'r')
    flux_lrt = f['flux_lrt'][...]
    wvl_lrt  = f['wvl_lrt'][...]
    f.close()

    flux_ssfr = flux_lrt.copy()
    ssfr_corr = np.zeros_like(sks.tmhr_corr)
    for i in range(flux_lrt.shape[0]):
        logic_nan = np.isnan(sks.spectra_flux_zen[i, :])
        sks.spectra_flux_zen[i, logic_nan] = -1.0
        logic_zen = (sks.spectra_flux_zen[i, :] < 0.0)
        if logic_zen.sum()<=30:
            logic_wvl_lrt  = (wvl_lrt>=sks.wvl_zen[10]) & (wvl_lrt<=sks.wvl_zen[-10])
            logic_wvl_ssfr = (sks.wvl_zen>=sks.wvl_zen[10]) & (sks.wvl_zen<=sks.wvl_zen[-10]) & (np.logical_not(logic_nan))
            flux_ssfr[i, logic_wvl_lrt] = sks.spectra_flux_zen[i, logic_wvl_ssfr]
            ssfr_corr[i] = np.trapz(flux_ssfr[i, :], wvl_lrt*1.0e-9)
            print(ssfr_corr[i])
            plt.plot(wvl_lrt, flux_ssfr[i, :])
            plt.plot(wvl_lrt, flux_lrt[i, :])
            plt.show()
            exit()
        else:
            ssfr_corr[i] = -1.0

    # wvlRange = [200, 4000]
    # indices  = (wvl_lrt>=wvlRange[0])&(wvl_lrt<=wvlRange[1])
    # wvl      = wvl_lrt[indices]

    # bbr_lrt  = np.zeros(flux_lrt.shape[0], dtype=np.float64)
    # for i in range(flux_lrt.shape[0]):
    #     flux       = flux_lrt[i, :][indices]
    #     bbr_lrt[i] = np.trapz(flux, x=wvl)

    bbr_corr = diff_ratio*bbr.zen_sw_flux_noturn+(1.0-diff_ratio)*bbr.zen_sw_flux_noturn*factor

    fig = plt.figure(figsize=(8, 6))
    ax1 = fig.add_subplot(111)
    # ax1.scatter(self.tmhr_corr, bbr_lrt, s=50, label='libRadtran'    , alpha=0.3, facecolor='none', edgecolor='k', linewidth=0.8)
    ax1.scatter(bbr.tmhr, bbr_corr, s=20, label='BBR'        , alpha=0.3, facecolor='none', edgecolor='r', linewidth=0.8)
    ax1.scatter(sks.tmhr_corr, ssfr_corr, s=20, label='SSFR'       , alpha=0.3, facecolor='none', edgecolor='b', linewidth=0.8)
    # ax1.scatter(bbr.tmhr, bbr.zen_sw_flux_noturn_attcorr , s=50, label='Anthony'     , alpha=0.3, facecolor='none', edgecolor='b', linewidth=0.8)
    ax1.set_xlim([25.0, 25.6])
    ax1.set_ylim([200,  600])
    ax1.set_xlabel('Time [hour]')
    ax1.set_ylabel('Flux [$\mathrm{W m^{-2}}$]')
    ax1.set_title('Zenith Shortwave Flux')
    ax1.legend(loc='best', fontsize=14, framealpha=0.4, markerscale=1)
    plt.savefig('comp_%3.1f.png' % p_offset)
    plt.show()
    exit()


    fig = plt.figure(figsize=(8, 6))
    ax1 = fig.add_subplot(111)
    ax1.set_xlim((25.0, 25.6))
    # ax1.set_xlabel('Time [hour]')
    # ax1.set_ylabel('Angle [$\mathrm{^\circ}$]')

    # ax1.scatter(self.tmhr_corr, sza0)
    # ax1.set_ylim((68, 72))
    # ax1.set_title('Solar Zenith Angle')
    # plt.savefig('cir2-sza.png')

    # ax1.scatter(self.tmhr_corr, saa0)
    # ax1.set_ylim((218, 230))
    # ax1.set_title('Solar Azimuth Angle')
    # plt.savefig('cir2-saa.png')

    # ax1.scatter(self.tmhr_corr, iza0)
    # ax1.set_ylim((0, 8))
    # ax1.set_title('Aircraft Zenith Angle')
    # plt.savefig('cir2-iza.png')

    # ax1.scatter(self.tmhr_corr, iaa0)
    # ax1.set_ylim((-180, 180))
    # ax1.set_title('Aircraft Azimuth Angle')
    # plt.savefig('cir2-iaa.png')

    ax1.scatter(self.tmhr_corr, dc*1200.0)
    # ax1.set_ylim((0.2, 0.5))
    ax1.set_ylim((200, 600))
    ax1.axis('off')
    # ax1.set_title('Slope $\mu \\times 1200$')
    plt.savefig('cir2-dc.png')
    # plt.show()
    exit()

def CSET_DC():

    import netCDF4 as nc4

    fname = 'CSET_HARP_20150719_HONG.nc'
    f = nc4.Dataset(fname, mode='r')

    iza0, iaa0 = PRH2ZA(f.variables['PITCH'][...], f.variables['ROLL'][...], f.variables['HEADING'][...])


    lon0  = f.variables['LON'][...]
    lat0  = f.variables['LAT'][...]
    alt0  = f.variables['ALTITUDE'][...]

    tmhr0 = np.zeros(lon0.size, dtype=np.float64)
    tmhr0[:] = f.variables['UTC'][...][:]

    sza0 = np.zeros_like(tmhr0)
    saa0 = np.zeros_like(tmhr0)
    for i in range(lon0.size):
        dtime_i  = datetime.datetime(2015, 7, 19) + datetime.timedelta(hours=tmhr0[i])
        dtime_i  = dtime_i.replace(tzinfo=datetime.timezone.utc)
        sza0[i]  = 90.0 - pysolar.solar.get_altitude(lat0[i], lon0[i], dtime_i, elevation=alt0[i])
        saa_i    = pysolar.solar.get_azimuth(lat0[i], lon0[i], dtime_i, elevation=alt0[i])
        if saa_i >= 0.0:
            if 0.0<=saa_i<=180.0:
                saa_i = 180.0 - saa_i
            elif 180.0<saa_i<=360.0:
                saa_i = 540.0 - saa_i
            else:
                print('Warning [CSET_DC]: invalid solar azimuth angle.')
        elif saa_i < 0.0:
            if -180.0<=saa_i<0.0:
                saa_i = -saa_i + 180.0
            elif -360.0<=saa_i<-180.0:
                saa_i = -saa_i - 180.0
            else:
                print('Warning [CSET_DC]: invalid solar azimuth angle.')
        saa0[i] = saa_i

    dc       = MUSLOPE(sza0, saa0, iza0, iaa0)

    fig = plt.figure(figsize=(8, 6))
    ax1 = fig.add_subplot(111)
    ax1.scatter(tmhr0,                      dc, s=2, label='Hong' , alpha=0.3, facecolor='none', edgecolor='r', linewidth=0.8)
    ax1.scatter(tmhr0,  f.variables['DC'][...], s=1, label='Sebastian', alpha=0.3, facecolor='none', edgecolor='b', linewidth=0.8)
    # ax1.scatter(tmhr0,  saa0                   , s=2, label='Hong' , alpha=0.3, facecolor='none', edgecolor='r', linewidth=0.8)
    # ax1.scatter(tmhr0,  f.variables['SAA'][...], s=2, label='Sebastian', alpha=0.3, facecolor='none', edgecolor='b', linewidth=0.8)
    # ax1.scatter(tmhr0,  sza0                   , s=2, label='Hong' , alpha=0.3, facecolor='none', edgecolor='r', linewidth=0.8)
    # ax1.scatter(tmhr0,  f.variables['SZA'][...], s=2, label='Sebastian', alpha=0.3, facecolor='none', edgecolor='b', linewidth=0.8)
    ax1.set_xlim([21.0, 24.0])
    ax1.set_ylim([0.0,  1.2])
    ax1.set_xlabel('Time [hour]')
    # ax1.set_ylabel('Flux [$\mathrm{W m^{-2}}$]')
    # ax1.set_title('Zenith Shortwave Flux')
    ax1.legend(loc='best', fontsize=14, framealpha=0.4, markerscale=1)
    plt.savefig('dc.png')
    plt.show()
    exit()


    fig = plt.figure(figsize=(8, 6))
    ax1 = fig.add_subplot(111)
    ax1.set_xlim((25.0, 25.6))
    # ax1.set_xlabel('Time [hour]')
    # ax1.set_ylabel('Angle [$\mathrm{^\circ}$]')

    # ax1.scatter(self.tmhr_corr, sza0)
    # ax1.set_ylim((68, 72))
    # ax1.set_title('Solar Zenith Angle')
    # plt.savefig('cir2-sza.png')

    # ax1.scatter(self.tmhr_corr, saa0)
    # ax1.set_ylim((218, 230))
    # ax1.set_title('Solar Azimuth Angle')
    # plt.savefig('cir2-saa.png')

    # ax1.scatter(self.tmhr_corr, iza0)
    # ax1.set_ylim((0, 8))
    # ax1.set_title('Aircraft Zenith Angle')
    # plt.savefig('cir2-iza.png')

    # ax1.scatter(self.tmhr_corr, iaa0)
    # ax1.set_ylim((-180, 180))
    # ax1.set_title('Aircraft Azimuth Angle')
    # plt.savefig('cir2-iaa.png')

    ax1.scatter(self.tmhr_corr, dc*1200.0)
    # ax1.set_ylim((0.2, 0.5))
    ax1.set_ylim((200, 600))
    ax1.axis('off')
    # ax1.set_title('Slope $\mu \\times 1200$')
    plt.savefig('cir2-dc.png')
    # plt.show()
    exit()

if __name__ == '__main__':

    import matplotlib as mpl
    from matplotlib.ticker import FixedLocator, MaxNLocator
    import matplotlib.gridspec as gridspec
    import matplotlib.pyplot as plt
    from matplotlib import rcParams

    fnames = sorted(glob.glob('data/ssfr/*.SKS'))
    sks    = READ_SKS_TEST(fnames, dateRef=datetime.datetime(2014, 10, 2), verbose=True)
    exit()

    fnames    = sorted(glob.glob('data/ssfr/*.plt2'))
    data_plt  = READ_PLT2(fnames, dateRef=datetime.datetime(2014, 10, 2), secOffset=20.0)
    BBR_COS_CORR(data_plt)
    # CSET_DC()

    exit()
